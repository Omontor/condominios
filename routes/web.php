<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('email', function() {

//     return new App\Mail\LoginCredentials(App\User::first(), 'asd123');
// });


Route::get('/', 'PagesController@home')->name('pages.home');
Route::get('nosotros', 'PagesController@about')->name('pages.about');
Route::get('archivo', 'PagesController@archive')->name('pages.archive');
Route::get('contacto', 'PagesController@contact')->name('pages.contact');
Route::get('staticpage', 'StaticPage@show')->name('staticpage.show');



Route::get('blog/{post}', 'PostsController@show')->name('posts.show');
Route::get('categorias/{category}', 'CategoriesController@show')->name('categories.show');
Route::get('tags/{tag}', 'TagsController@show')->name('tags.show');


Route::get('login/{socialNetwork}', 'SocialLoginController@redirectToSocialNetwork')->name('login.social')->middleware('guest');
Route::get('login/{SocialNetwork}/callback', 'SocialLoginController@handleSocialNetworkCallback')->middleware('guest');

//Métodos con el prefijo Admin

Route::group(['prefix' => 'admin', 
    'namespace' => 'Admin', 
    'middleware' => 'auth'], 
    function() {
        //rutas de administración
    Route::get('/','AdminController@index')->name('dashboard');


    Route::resource('media', 'MediaController', ['except' => 'show', 'as' => 'admin']);
    Route::resource('condos', 'CondosController', ['except' => '', 'as' => 'admin']);
    Route::resource('properties', 'PropertiesController', ['except' => '', 'as' => 'admin']);
    Route::resource('debts', 'DebtsController', ['except' => '', 'as' => 'admin']);
    Route::resource('members', 'MembersController', ['except' => '', 'as' => 'admin']);
    Route::resource('visitors', 'VisitorsController', ['except' => '', 'as' => 'admin']);  
    Route::resource('payments', 'PaymentsController', ['except' => '', 'as' => 'admin']);          
    Route::resource('incidents', 'IncidentsController', ['except' => '', 'as' => 'admin']);
    Route::resource('reservations', 'ReservationsController', ['except' => '', 'as' => 'admin']);
    Route::resource('events', 'EventsController', ['except' => '', 'as' => 'admin']);
    Route::resource('commonareas', 'CommonAreasController', ['except' => '', 'as' => 'admin']);
    Route::resource('visits', 'VisitsController', ['except' => '', 'as' => 'admin']);
    Route::resource('listings', 'ListingsController', ['except' => '', 'as' => 'admin']);
    Route::resource('divisions', 'DivisionsController', ['except' => '', 'as' => 'admin']);
    Route::resource('subdivisions', 'SubdivisionsController', ['except' => '', 'as' => 'admin']);
    Route::resource('services', 'ServicesController', ['except' => '', 'as' => 'admin']);

    Route::get('listings.categories', 'ListingsController@categories')->name('admin.listings.categories');
    Route::get('listings.subcategories', 'ListingsController@subcategories')->name('admin.listings.subcategories');
    Route::get('listings.listings', 'ListingsController@listings')->name('admin.listings.listings');




    Route::resource('contacts', 'ContactController', ['except' => 'show', 'as' => 'admin']);
    Route::resource('page', 'PageController', ['except' => '', 'as' => 'admin']);
    Route::resource('projects', 'ProjectController', ['except' => 'show', 'as' => 'admin']);
    Route::get('procjects/goback', 'ProjectController@goback')->name('admin.projects.goback');


    Route::resource('setting', 'SettingController', ['except' => '', 'as' => 'admin']);
    Route::resource('posts', 'PostsController', ['except' => 'show', 'as' => 'admin']);
    Route::resource('users', 'UsersController', [ 'as' => 'admin']);
    Route::resource('roles', 'RolesController', ['except' => 'show', 'as' => 'admin']);
    Route::resource('permissions', 'PermissionsController', ['only' => ['index', 'edit', 'update'], 'as' => 'admin']);

    Route::middleware('role:Admin')->put('users/{user}/roles', 'UsersRolesController@update')->name('admin.users.roles.update');

    Route::middleware('role:Admin')->put('users/{user}/permissions', 'UsersPermissionController@update')->name('admin.users.permissions.update');


    Route::post('posts/{post}/photos', 'PhotosController@store')->name('admin.posts.photos.store');     
    Route::delete('photos/{photo}', 'PhotosController@destroy')->name('admin.photos.destroy');

    Route::post('projects/{project}/projectphotos', 'ProjectPhotosController@store')->name('admin.projectphotos.projectphotos.store'); 

    Route::post('projects/{project}/projectheader', 'ProjectPhotosController@header')->name('admin.projectphotos.projectphotos.header'); 

    Route::delete('projectphotos/{projectphoto}', 'ProjectPhotosController@destroy')->name('admin.projectphotos.destroy');     

    Route::post('condos/{condo}/condologos', 'CondosLogosController@store')->name('admin.condos.condologos.store');

    Route::delete('condologos/{condologo}', 'CondosLogosController@destroy')->name('admin.condologos.destroy');
 
    Route::post('condos/{condo}/condophotos', 'CondosPhotosController@store')->name('admin.condos.condophotos.store');

    Route::delete('condophotos/{condophoto}', 'CondosPhotosController@destroy')->name('admin.condophotos.destroy');

});





//Quitar para desactivar registro
Auth::routes();
        // Authentication Routes...
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');

        Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'Auth\RegisterController@register');

        Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');