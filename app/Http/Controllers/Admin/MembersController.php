<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Member;
use App\User;
use App\Property;

class MembersController extends Controller
{
 	
    public function index () {

    	$users = User::all();
        $members = Member::all();
        $properties = Property::all();

    	return view('admin.members.index', compact('members', 'users', 'properties'));
    }

        public function show (Member $member) {
		return view('admin.members.show', compact('members'));
    }


        public function store(Request $request) {

        $this->validate($request,[
        'property_id' => 'required',
        'user_id' => 'required',

    ]);
        $member = Member::create($request->all());
        return redirect()->route('admin.members.index')->with('flash', 'El Habitante Fue Asociado Correctamente');

    }     

        public function destroy(Listing $listing){
        
        $listing->delete();
         return redirect()->route('admin.listings.index')->with('flash', 'El Habitante ha sido Eliminado');

    }
}
