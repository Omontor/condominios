<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Event;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;



class EventsController extends Controller
{
	public function index()

	{


        $events = Event::all();
    	return view('admin.events.index', compact('events'));

	}



}
