<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Property;
use App\Condo;
use App\Debt;
use App\Payment;
use App\Visitor;
use App\Post;
use App\Category;
use App\Project;
use App\ProjectCategory;
use App\Listing;
use App\Division;
use App\Subdivision;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.dashboard', [

                 'users' => User::all()->count(),
                 'condos' => Condo::latest()->take(5)->get(),
                 'condoscount' => Condo::all()->count(),                
                 'posts' => Post::all()->count(),
                 'categories' => Category::all()->count(),
                 'projects' => Project::all()->count(),
                 'projectcategories' => ProjectCategory::all()->count(),
                 'expected_income' => Property::all()->sum('monthly_maintenance'),
                 'propertiescount' => Property::all()->count(),
                 'properties' => Property::latest()->take(10)->get(),
                 'propertiescount' => Property::all()->count(),
                 'memberscount' => User::whereHas('roles', function($q){$q->where('name', 'Member');})->get()->count(),
                 'visitorscount' => Visitor::all()->count(),
                 'debtscount'=> Debt::all()->count(),
                 'debts'=> Debt::all()->sum('monthly_maintenance'),
                 'paymentscount' => Payment::all()->sum('payment_amount'),
                 'listingscount'=> Listing::all()->count(), 
                 'listings'=> Listing::latest()->take(5)->get(), 
                 'divisionscount'=> Division::all()->count(),  
                 'subdivisionscount'=> Subdivision::all()->count(),  

             ]);
    }
}
