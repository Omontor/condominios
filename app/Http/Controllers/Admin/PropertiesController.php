<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Condo;
use App\Property;
use App\Member;
use App\Http\Requests\StorepropertyRequest;

class PropertiesController extends Controller
{
    public function index () {

        $condos = Condo::all();
        $properties = Property::all();
        $members = Member::all();
    	return view('admin.properties.index', compact('properties', 'condos'));
    }

        public function create () {

        $condos = Condo::all();
    	return view('admin.properties.create', compact('condos'));
    }

    public function store(Request $request) {

        $condos = Condo::all();
        $this->validate($request,[
        'name' => '|requiredmin:3'
    ]);
        $property = Property::create($request->all());
        return redirect()->route('admin.properties.edit', $property);

    }

        public function edit(Property $property){
            
                $condos = Condo::all();
                $currentcondo = Condo::where('id', '=', $property->condo_id)->get();

            return view('admin.properties.edit',[
                    
            'property' => $property,
            'condos' => $condos,
            'currentcondo' => $currentcondo,
            ]);
    }

    public function update (Property $property, StorepropertyRequest $request) {


         $property->update($request->all());
    	 return redirect()->route('admin.properties.edit', $property)->with('flash', 'La Propiedad Fue Actualizada Correctamente');

    } 


    public function destroy(Property $property){
        
        $property->delete();
         return redirect()->route('admin.properties.index', $property)->with('flash', 'La publicación ha sido eliminada');

    }


}
