<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StorepageRequest;


class PageController extends Controller
{
    public function index()
    {
    
        $pages = Page::all();
        return view('admin.pages.index', compact('pages'));
    }

    public function store(Request $request) {
        $this->validate($request,[
            'title' => 'required|min:3']);
        $page = Page::create($request->all());


        return redirect()->route('admin.pages.edit', $page);

    }

    public function edit(Page $page){
            

            return view('admin.pages.edit',[         
            'page' => $page
            ]);
    }

    public function update (Page $page, StorepageRequest $request) {


        $page->update($request->all());
    	return redirect()->route('admin.page.edit', $page)->with('flash', 'La publicación ha sido guardada');

    }


    public function destroy(Page $page){
        
        $this->authorize('delete', $page);
        $page->delete();
         return redirect()->route('admin.pages.index', $page)->with('flash', 'La publicación ha sido eliminada');

    }

    
}
