<?php

namespace App\Http\Controllers\Admin;
use App\Division;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DivisionsController extends Controller
{
    public function store(Request $request) {


        $this->validate($request,[
            'name' => 'required|min:3']);
        $divisions = Division::create($request->all());
        return redirect()->route('admin.listings.categories')->with('flash', 'La Categoría ha sido guardada');;

    }
}
