<?php

namespace App\Http\Controllers\Admin;

use App\Condo;
use App\CommonArea;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCondoRequest;


class CondosController extends Controller
{
    public function index () {


        $condos = Condo::all();
    	return view('admin.condos.index', compact('condos'));
    }

    public function create () {

    //	$categories = Category::all();
    	$commonareas = CommonArea::all();
    	return view('admin.condos.create', compact('commonareas'));
    }

    public function store(Request $request) {


        $this->validate($request,[

            'name' => 'required|min:3']);

        $condo = Condo::create($request->all());


        return redirect()->route('admin.condos.edit', $condo);

    }

        public function edit(Condo $condo){
            
 
            return view('admin.condos.edit',[
                    
            'commonareas' => CommonArea::all(),
            'condo' => $condo,
            ]);
    }

    public function update (Condo $condo, StoreCondoRequest $request) {


        $condo->update($request->all());
        $condo->synccommonareas($request->get('common_areas'));

    	 return redirect()->route('admin.condos.edit', $condo)->with('flash', 'El Condominio Ha Sido Guardado');

    } 


    public function destroy(Condo $condo){
        
        $condo->delete();
         return redirect()->route('admin.condos.index', $condo)->with('flash', 'La publicación ha sido eliminada');

    }
      

}
