<?php

namespace App\Http\Controllers\Admin;
use App\Subdivision;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SubdivisionsController extends Controller
{
    public function store(Request $request) {


        $this->validate($request,[
            'name' => 'required|min:3']);
        $subdivisions = Subdivision::create($request->all());
        return redirect()->route('admin.listings.subcategories')->with('flash', 'La Categoría ha sido guardada');;

    }
}
