<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Visit;
use App\Service;

class VisitsController extends Controller
{
    public function index () {


        $visits = Visit::all();
        $services = Service::all();
    	return view('admin.visits.index', compact('visits' , 'services'));
    }}
