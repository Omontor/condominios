<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Photo;
use App\Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhotosController extends Controller
{
    public function store (Post $post){

    	$this->validate(request(), [

    		'photo' => 'required'
    	]);

        $post->photos()->create([
        'url' => "uploads/" . request()->file('photo')->store('posts'),

        ]);


        $media = Media::create([
            'url' => "uploads/" . request()->file('photo')->store('posts'),

        ]); 

    }


    public function destroy(Photo $photo){


            $res=Media::where('url',$photo->url)->delete();
            $photo->delete();
            return back()->with('flash', 'Foto Eliminada');
    }
}
