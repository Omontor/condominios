<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service;

class ServicesController extends Controller
{
       public function store(Request $request) {

        $this->validate($request,[
        'name' => 'required',
    ]);
        $services = Service::create($request->all());
        return redirect()->route('admin.visits.index')->with('flash', 'El Servicio Fue Creado Correctamente');

    } 
}
