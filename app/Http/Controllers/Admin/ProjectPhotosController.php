<?php

namespace App\Http\Controllers\Admin;

use App\ProjectPhoto;
use App\Project;
use App\Media;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProjectPhotosController extends Controller
{
    public function store(Project $project)
    {

        $this->validate(request(), [

            'photo' => 'required'
        ]);

        $project->projectphotos()->create([
        'url' => "uploads/" . request()->file('photo')->store('projects'),
                'isHeader' => '0',
        ]);

          $media = Media::create([
            'url' => "uploads/" . request()->file('photo')->store('projects'),
        ]);

    }

    public function header(Project $project)
    {

        $this->validate(request(), [

            'photo' => 'required'
        ]);

        $project->projectphotos()->create([
        'url' => "uploads/" . request()->file('photo')->store('projects'),
        'isHeader' => '1',
        ]);
    }


    public function destroy(ProjectPhoto $projectphoto)
    {

            $res=Media::where('url',$projectphoto->url)->delete();
            $projectphoto->delete();
            return back()->with('flash', 'Foto Eliminada');
    }
}
