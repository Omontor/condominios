<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Listing;
use App\Division;
use App\Subdivision;

class ListingsController extends Controller
{
      public function categories () {

        $divisions = Division::all();
    	return view('admin.listings.categories', compact('divisions'));
    }

      public function subcategories () {
        $divisions = Division::all();
        $subdivisions = Subdivision::orderBy('id', 'desc')->get();
    	return view('admin.listings.subcategories', compact('subdivisions', 'divisions'));
    }

      public function listings () {
        $subdivisions = Subdivision::all();
        $listings = Listing::orderBy('id', 'desc')->get();
    	return view('admin.listings.listings', compact('listings', 'subdivisions'));
    }   



    public function store(Request $request) {

        $subdivisions = Subdivision::all();
        $this->validate($request,[
        
    ]);
        $listing = Listing::create($request->all());
        return redirect()->route('admin.listings.listings', $listing)->with('flash', 'El Anunciante Fue Creado Correctamente');

    }     


        public function destroy(Listing $listing){
        
        $listing->delete();
         return redirect()->route('admin.listings.listings', $listing)->with('flash', 'El anunciante ha sido eliminado');

    }
}
