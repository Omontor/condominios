<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Visitor;
use App\Property;

class VisitorsController extends Controller
{
    public function index () {

    	$properties = Property::all();
        $visitors = Visitor::orderBy('id', 'desc')->get();;
    	return view('admin.visitors.index', compact('visitors', 'properties'));
    }

        public function show (Property $property) {
		return view('admin.visitors.show', compact('visitors'));
    }



       public function store(Request $request) {

        $this->validate($request,[
        'name' => 'required',
        'property_id' => 'required',
        'next_visit' => 'required',
    ]);
        $visitor = Visitor::create($request->all());
        $visitor->ine_url = '/img/user.png';
        return redirect()->route('admin.visitors.index')->with('flash', 'El Visitante Fue Creado Correctamente');

    }     

        public function destroy(Listing $visitor){
        
        $visitor->delete();
         return redirect()->route('admin.visitors.index')->with('flash', 'El Visitante ha sido Eliminado');

    }
}
