<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Condo;
use App\Media;
use App\CondoLogo;

class CondosLogosController extends Controller
{
    public function store (Condo $condo){

    	$this->validate(request(), [

    		'photo' => 'required'
    	]);

        $condo->condologos()->create([
        'url' => "uploads/" . request()->file('photo')->store('condo_logos'),

        ]);


        $condo->logo_url = "uploads/" . request()->file('photo')->store('condo_logos');
    }


    public function destroy(CondoLogo $condologo){


            $res=Media::where('url',$condologo->url)->delete();
            $condologo->delete();
            return back()->with('flash', 'Foto Eliminada');
    }
}
