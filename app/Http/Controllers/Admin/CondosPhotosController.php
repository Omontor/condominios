<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Condo;
use App\Media;
use App\CondoPhoto;
class CondosPhotosController extends Controller
{
 public function store (Condo $condo){

    	$this->validate(request(), [

    		'photo' => 'required'
    	]);

        $condo->condophotos()->create([
        'url' => "uploads/" . request()->file('photo')->store('condo_photos'),

        ]);


        $condo->logo_url = "uploads/" . request()->file('photo')->store('condo_photos');
    }


    public function destroy(CondoPhoto $condophoto){


            $res=Media::where('url',$condophoto->url)->delete();
            $condophoto->delete();
            return back()->with('flash', 'Foto Eliminada');
    }
}
