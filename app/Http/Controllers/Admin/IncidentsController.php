<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Incident;

class IncidentsController extends Controller
{
    public function index () {


        $incidents = Incident::all();
    	return view('admin.incidents.index', compact('incidents'));
    }

        public function show (Incident $incident) {
		return view('admin.incidents.show', compact('incidents'));
    }
}
