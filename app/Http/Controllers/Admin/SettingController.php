<?php

namespace App\Http\Controllers\Admin;

use App\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingController extends Controller
{

    public function index()
    {
    
        $setting = Setting::all();
        return view('admin.setting.index', compact('setting'));
    }


        public function update (Setting $setting, request $request) 

        {

        $setting->update($request->all());

        return redirect()->route('admin.setting.index', $setting)->with('flash', 'La configuración ha sido guardada');

    }



}
