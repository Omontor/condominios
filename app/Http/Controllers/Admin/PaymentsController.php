<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Payment;


class PaymentsController extends Controller
{
    public function index () {
        $payments = Payment::all();
    	return view('admin.payments.index', compact('payments'));
    }

        public function show (Payment $payment) {
		return view('admin.payments.show', compact('payments'));
    }
}
