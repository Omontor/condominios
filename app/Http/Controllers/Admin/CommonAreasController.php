<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CommonArea;

class CommonAreasController extends Controller
{
    public function index () {


        $commonareas = CommonArea::all();
    	return view('admin.commonareas.index', compact('commonareas'));
    }


    public function store(Request $request) {


        $this->validate($request,[

            'name' => 'required|min:3']);

        $commonarea = commonarea::create($request->all());
        return redirect()->route('admin.commonareas.index')->with('flash', 'Área Común Ha Sido Guardada');

    }

}
