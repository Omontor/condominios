<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Debt;

class DebtsController extends Controller
{
    public function index () {


        $debts = Debt::all();
    	return view('admin.debts.index', compact('debts'));
    }

        public function show (Debt $debt) {
		return view('admin.debts.show', compact('debts'));
    }
}
