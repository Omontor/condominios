<?php
                    

function setActiveRoute ($name)
{
	 return request()->routeIs($name) ? 'active' : '';
}

function setActiveTreeview ($name)
{
	 return request()->routeIs($name) ? 'menu-open' : '';
}

