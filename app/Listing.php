<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
	protected $guarded = [];
	public function review() {

		return $this->hasMany(Review::class);
	}

		public function subdivision() {

		return $this->belongsTo(SubDivision::class);
	}
}
