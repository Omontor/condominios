<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condo extends Model
{
    protected $fillable = [
 		'name', 'logo_url', 'photo_url', 'iframe', 'address', 'lat', 'lng', 'primary_color', 'secondary_color', 'monthly_maintenance'
    ];

	public function properties() {

		return $this->hasMany(Property::class);
	}

	public function common_areas () {


		return $this->belongsToMany(CommonArea::class);

	}

		public function condologos() {


		return $this->hasMany(Condologo::class);
	}

		public function condophotos() {


		return $this->hasMany(CondoPhoto::class);
	}


		public function synccommonareas($commonareas)
	{

		 $commonareasids = collect($commonareas)->map(function($commonarea){

            return CommonArea::find($commonarea) ? $commonarea : CommonArea::create(['name' => $commonarea])->id;

        });

	   return	$this->common_areas()->sync($commonareasids);


	} 


}
