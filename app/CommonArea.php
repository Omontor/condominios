<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommonArea extends Model
{
    protected $guarded = [];
}
