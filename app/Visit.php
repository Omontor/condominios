<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{


	public function service () {
		return $this->belongsToMany(Service::class);

	}


		public function property(){

		return $this->belongsTo(Property::class);

	}


}
