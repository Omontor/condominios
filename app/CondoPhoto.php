<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use File;
class CondoPhoto extends Model
{
    protected $guarded = [];

    protected static function boot(){

    	parent::boot();

    	static::deleting(function($condophoto){

			File::delete(public_path("/". $condophoto->url));
    	});

		}
}
