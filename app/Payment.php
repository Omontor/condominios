<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
  	public function property(){

		return $this->belongsTo(Property::class);

	}
}
