<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{

    protected $fillable = [
 		'condo_id', 'address', 'area', 'members', 'monthly_maintenance',
    ];

	public function condo(){

		return $this->belongsTo(Condo::class);

	}

	public function debts() {

		return $this->hasMany(Debt::class);

	}

	public function payments() {

		return $this->hasMany(Payment::class);

	}

		public function member() {

		return $this->hasMany(Member::class);

	}


		public function visitor() {

		return $this->hasMany(Visitor::class);

		}




}
