<?php

namespace App;

use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class CondoLogo extends Model
{
    protected $guarded = [];

    protected static function boot(){

    	parent::boot();

    	static::deleting(function($condologo){

			File::delete(public_path("/". $condologo->url));
    	});

		}

}
