<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subdivision extends Model
{

	protected $guarded = [];
	
	public function listing() {

		return $this->hasMany(Listing::class);
	}

		public function division() {

		return $this->belongsTo(Division::class);
	}
}
