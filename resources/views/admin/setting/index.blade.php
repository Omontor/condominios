@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Editar Configuración del Sitio</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Crear</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')
<form method="POST" action="{{route('admin.setting.update', $setting->first())}}">
  {{csrf_field()}} {{method_field('PUT')}}
 <div class="row mb-2">
  
<div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
            </div>         
                <div class="card-body">
              <h2>Configuración General</h2>
              <br>
              <div class="form-group {{$errors->has('site_name') ? 'is-invalid' : ''}}">
                <label>Título del Sitio</label>
                <input name="site_name" 
                class="form-control" 
                placeholder="Ingresa el Título de la Publicación" 
                value="{{old('title', $setting->first()->site_name)}}">
                <br>
                </input>
                <label>Descripción del Sitio</label>
                        <textarea  name="site_desc" rows="5"  style="width: 100%;">{{old('site_desc', $setting->first()->site_desc)}}
                    </textarea>
                    {!!$errors->first('site_desc', ' <span class="help-block" style= "color:red;">:message</span>')!!}
              </div>

              <h2>Configuración de email</h2>
              <br>
  <div class="row">
              <div class="col-md-6 col-sm-12">
                 <label>email de Administración</label>
                              <input name="admin_email" 
                              class="form-control" 
                              placeholder="Ingresa el Título de la Publicación" 
                              value="{{old('admin_email', $setting->first()->admin_email)}}">
                              <br>
                              </input>
              </div>
              <div class="col-md-6 col-sm-12">
                 <label>Nombre del Administrador</label>
                              <input name="admin_name" 
                              class="form-control" 
                              placeholder="Ingresa el Título de la Publicación" 
                              value="{{old('admin_name', $setting->first()->admin_name)}}">
                              <br>
                              </input>
              </div>


<div class="col-md-6 col-sm-12">
                 <label>email "De:"</label>
                              <input name="email_from" 
                              class="form-control" 
                              placeholder="Ingresa el Título de la Publicación" 
                              value="{{old('email_from', $setting->first()->email_from)}}">
                              <br>
                              </input>
              </div>
              <div class="col-md-6 col-sm-12">
                 <label>Host de Correo de Salida</label>
                              <input name="email_host" 
                              class="form-control" 
                              placeholder="Ingresa el Título de la Publicación" 
                              value="{{old('email_host', $setting->first()->email_host)}}">
                              <br>
                              </input>
              </div>

<div class="col-md-6 col-sm-12">
                 <label>puerto</label>
                              <input name="email_port" 
                              class="form-control" 
                              placeholder="Ingresa el Título de la Publicación" 
                              value="{{old('email_port', $setting->first()->email_port)}}">
                              <br>
                              </input>
              </div>
              <div class="col-md-6 col-sm-12">
                 <label>Email de Salida</label>
                              <input name="email_username" 
                              class="form-control" 
                              placeholder="Ingresa el Título de la Publicación" 
                              value="{{old('email_username', $setting->first()->email_username)}}">
                              <br>
                              </input>
              </div>

                            <div class="col-md-12">
                 <label>Password email de Salida</label>
                              <input name="email_password" 
                              class="form-control" 
                              placeholder="Ingresa el Título de la Publicación" 
                              value="{{old('email_password', $setting->first()->email_password)}}">
                              <br>
                              </input>
              </div>
  </div>

              <div class="form-group">
                
                <button type="submit" class="btn btn-primary btn-block">
                  Guardar Configuración 
                </button>
              </div>

  </form>
              </div>   
          </div>

        </div>





@endsection


@push('styles')

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- daterange picker -->
  <link rel="stylesheet" href="/adminlte/plugins/daterangepicker/daterangepicker.css">
    <!-- Select2 -->
  <link rel="stylesheet" href="/adminlte/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endpush

@push('scripts')


@endpush

