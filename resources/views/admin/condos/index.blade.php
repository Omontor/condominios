@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Condominios</h1>
            <small>Listado</small>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Condominios</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')
          <div class="card">
            <div class="card-header" style="vertical-align:  middle;">
              <h3 class="card-title" style="vertical-align:  middle;">Listado de Condominios</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">


<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    var markers = [

                @forelse($condos as $condo)

    {     
        "title": '{{$condo->name}}}',
        "lat": '{{$condo->lat}}',
        "lng": '{{$condo->lng}}',
        "description": '{{$condo->name}}'
    },
    @empty
    @endforelse
    
    ];
    window.onload = function () {
        LoadMap();
    }
    function LoadMap() {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            // zoom: 8, //Not required.
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
 
        //Create LatLngBounds object.
        var latlngbounds = new google.maps.LatLngBounds();
 
        for (var i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);
 
            //Extend each marker's position in LatLngBounds object.
            latlngbounds.extend(marker.position);
        }
 
        //Get the boundaries of the Map.
        var bounds = new google.maps.LatLngBounds();
 
        //Center map and adjust Zoom based on the position of all markers.
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
    }
</script>
<div id="dvMap" style="width: 100%; height: 400px">
</div>


<script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-VZUr01EdXUPZfUcj_UilKvo1DjmfGG0&callback=initMap">
                        </script>
                        <br><br>



              <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Logo</th>
                  <th>Nombre</th>
                  <th>Dirección</th>
                  <th>Mantenimiento Mensual</th>
                  <th>Áreas Comunes</th>
                  <th>Propiedades</th>
                  <th>Viviendas</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($condos as $condo)
               <tr>
                 <td style="align-content: center; vertical-align: middle;">
                <center>  {{$condo->id}}</center>
                </td>
                  <td style="align-content: center; vertical-align: middle;"> 
                    <center>
                        @if($condo->condologos->count())
                        <img src="/{{$condo->condologos->first()->url}}" width="150px">
                        @else
                      <img src="/img/villa.png" width="50px">
                      @endif
                    </center>
                  </td>
                  <td style="align-content: center; vertical-align: middle;">{{$condo->name}}</td>
                  <td style="align-content: center; vertical-align: middle;">{{$condo->address}}</td>
                  <td style="align-content: center; vertical-align: middle;"><center>${{$condo->monthly_maintenance}}</center></td>
                  <td style="align-content: center; vertical-align: middle;"><center>

                    @forelse($condo->common_areas as $common_area)
                      <ul>
                        <li>{{$common_area->name}} </li>
                      </ul>

                    @empty  
                    La propiedad no tiene asignadas áreas comunes
                    @endforelse

                  </center></td>
                  <td style="align-content: center; vertical-align: middle;"><center>{{count($condo->properties)}}</center></td>
                  <td style="align-content: center; vertical-align: middle;">
<center>

                    <a class="btn btn-success btn-sm" href="{{route('admin.condos.edit', $condo)}}"><i class="fas fa-home" alt="Viviendas"></i></a>
                    <a class="btn btn-dark btn-sm" href="{{route('admin.condos.edit', $condo)}}"><i class="fas fa-cog"></i></a>
                    <a class="btn btn-primary btn-sm" href="{{route('admin.condos.show', $condo)}}"><i class="fas fa-eye"></i></a>
                    <a class="btn btn-info btn-sm" href="{{route('admin.condos.edit', $condo)}}"><i class="fas fa-pencil-alt"></i></a>
                    <form method="POST" action="{{route('admin.condos.destroy', $condo)}}" style="display: inline;"> 
                      {{ csrf_field() }} {{method_field('DELETE')}}
                      
                    <button class="btn btn-danger btn-sm" href="" onclick="return confirm('Estás Seguro de Querer Eliminar Esta Publicación?')"><i class="fas fa-times"></i></button>

                    </form>
        </center>              

                  </td>

                   </tr>
                  @empty
                  <tr>
                    <td>No hay datos para mostrar</td>
                  </tr>
                  @endforelse
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
@endsection



@push('styles')
    <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#posts-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>




@endpush