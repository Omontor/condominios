<!-- Modal -->
<div class="modal fade" id="exampleModalLong2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <form method="POST" action="{{route('admin.condos.store', '#create')}}">
  {{csrf_field()}}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Crear Condominio Nuevo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <div class="form-group {{$errors->has('title') ? 'is-invalid' : ''}}">
                <input id= "name" name="name" 
                class="form-control" 
                placeholder="Ingresa el Nombre del Condominio" 
                value="{{old('title')}}" autofocus required>
                </input>
               {!!$errors->first('title', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button  class="btn btn-primary">Crear Condominio</button>
      </div>
    </div>
  </div>
</form>
</div>

@push('scripts')
<script>
  
  if (window.location.hash === '#create')
  {

    $('#exampleModalLong2').modal('show');


  }

      $('#exampleModalLong2').on('hide.bs.modal', function(){

        window.location.hash = '#';

      });

            $('#exampleModalLong2').on('shown.bs.modal', function(){

        $('#post-title').focus();
        window.location.hash = '#create';

      });

</script>
@endpush