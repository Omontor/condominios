@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Crear Condominio</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item"><a href="{{route('admin.condos.index')}}">Condominios</a></li>
              <li class="breadcrumb-item active">Crear</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')
<form method="POST" action="{{route('admin.condos.update', $condo)}}">
  {{csrf_field()}} {{method_field('PUT')}}
 <div class="row mb-2">
  
<div class="col-md-8">
        <div class="card card-primary">
            <div class="card-header">
            </div>         
              <div class="card-body">
<div class="row">
                <div class="col-6">

          <div class="form-group {{$errors->has('name') ? 'is-invalid' : ''}}">
                <label>Nombre del Condominio</label>
                <input name="name" 
                class="form-control" 
                placeholder="Ingresa el Título de la Publicación" 
                value="{{old('name', $condo->name)}}">
                </input>
               {!!$errors->first('name', ' <span class="help-block" style= "color:red;">:message</span>')!!}


                      </div>


           <div class="form-group {{$errors->has('address') ? 'is-invalid' : ''}}">
                <label>Dirección</label>
                <input name="address" 
                class="form-control" 
                placeholder="Ej. San Borja 633 Colonia del Valle, CDMX" 
                value="{{old('address', $condo->address)}}">
                </input>
               {!!$errors->first('address', ' <span class="help-block" style= "color:red;">:message</span>')!!}
            </div>

           <div class="form-group {{$errors->has('monthly_maintenance') ? 'is-invalid' : ''}}">
                <label>Mantenimiento Mensual</label>
                <input name="monthly_maintenance" 
                class="form-control" 
                placeholder="Ej. San Borja 633 Colonia del Valle, CDMX" 
                value="{{old('monthly_maintenance', $condo->monthly_maintenance)}}">
                </input>
               {!!$errors->first('monthly_maintenance', ' <span class="help-block" style= "color:red;">:message</span>')!!}
            </div>

               </div>

<div class="col-6">
  <label>Logotipo</label>
<br>
<center>
  @if($condo->condologos->count())
            <img src="/{{$condo->condologos->first()->url}}" class="img-thumbnail">
                  
  @else
  <img src="/img/villa.png"  style="max-width: 200px; max-height: 200px;">
  @endif
</center>

</div>
</div>
<br>
<hr>
<div class="row">
                
<div class="col-8">
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    var markers = [



    {     
        "title": '{{$condo->name}}}',
        "lat": '{{$condo->lat}}',
        "lng": '{{$condo->lng}}',
        "description": '{{$condo->name}}'
    },

    
    ];
    window.onload = function () {
        LoadMap();
    }
    function LoadMap() {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
             zoom: 18, //Not required.
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
 
 
        for (var i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);
 
            //Extend each marker's position in LatLngBounds object.
            latlngbounds.extend(marker.position);
        }
 
        //Get the boundaries of the Map.
        var bounds = new google.maps.LatLngBounds();
 
        //Center map and adjust Zoom based on the position of all markers.
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
    }
</script>
<div id="dvMap" style="width: 100%; height: 400px">
</div>


<script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-VZUr01EdXUPZfUcj_UilKvo1DjmfGG0&callback=initMap">
                        </script>
                        <br><br>

  </div>

  <div class="col-4">
<div class="form-group {{$errors->has('lat') ? 'is-invalid' : ''}}">
  <h4>Coordenadas</h4>
                <label>Latitud</label>
                <input name="lat" 
                class="form-control" 
                placeholder="Lat" 
                value="{{old('lat', $condo->lat)}}">
                </input>
               {!!$errors->first('lat', ' <span class="help-block" style= "color:red;">:message</span>')!!}


                      </div>


           <div class="form-group {{$errors->has('lng') ? 'is-invalid' : ''}}">
                <label>Longitud</label>
                <input name="lng" 
                class="form-control" 
                placeholder="Lng" 
                value="{{old('lng', $condo->lng)}}">
                </input>
               {!!$errors->first('lng', ' <span class="help-block" style= "color:red;">:message</span>')!!}


                      </div>
  </div>
</div>

              </div>  

          </div>

        </div>

            <div class="col-md-4">
        <div class="card card-primary">
            <div class="card-header">
            </div>
            <div class="card-body">


                <div class="form-group {{$errors->has('commonareas') ? 'is-invalid' : ''}}">
                  <label>Áreas Comunes</label>
                    <select name="common_areas[]" class="select2" multiple="multiple" data-placeholder="Selecciona Una o Más Áreas Comunes" style="width: 100%;">

                      @forelse($commonareas as $commonarea)
                      <option {{collect(old('common_areas', $condo->common_areas->pluck('id')))->contains($commonarea->id) ? 'selected' : ''}} value="{{$commonarea->id}}">{{$commonarea->name}}</option>
                      @empty
                      @endforelse
                  </select>
                  {!!$errors->first('common_areas', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>  

 <div class="form-group {{$errors->has('primary_color') ? 'is-invalid' : ''}}"  >
                    <label>Color Primario</label>
                  <input type="text" class="form-control my-colorpicker1" name="primary_color" value="{{old('primary_color', $condo->primary_color)}}">
  </div>

   <div class="form-group {{$errors->has('secondary_color') ? 'is-invalid' : ''}}" >
                    <label>Color Secundario</label>
                  <input type="text" class="form-control my-colorpicker3" name="secondary_color" value="{{old('secondary_color', $condo->secondary_color)}}" >
  </div>          

<div class="form-group">
    @if(!$condo->condologos->count())
  <label>Logotipo</label>
  <div class="dropzone" id="dropzone">
    </div>
    @else
     <div class="dropzone" id="dropzone" style="display: none;">
    </div>
    @endif

</div>
                  <!-- /.input group -->
               
              <div class="form-group">
                @if($condo->condophotos->count() >= 4)
              	 <label>Galería</label>
                  <div class="dropzone" id="dropzone2"  style="display: none;">                 
                  @else
                 <div class="dropzone" id="dropzone2">
              
              		@endif
              	</div>
              </div>

              <div class="form-group">
                
                <button type="submit" class="btn btn-primary btn-block">
                  Guardar Condominio
                </button>
              </div>

  </form>
            </div>
          </div>
    </div>

<div class="col-md-3">
  <div class="card card-primary">
      <div class="card-header">
      </div>
    <div class="card-body"> 
      <label>Logotipo</label>
        <div class="container"><div class="row">
    @if($condo->condologos->count())
     @foreach($condo->condologos as $photo)
    <div class="col-12">
      <form method="POST" action="{{route('admin.condologos.destroy', $photo)}}">
                {{method_field('DELETE')}} {{csrf_field()}}
                <button class="btn btn-danger btn-md" style="position: absolute;"><i class="fa fa-trash"></i></button>
                <img src="{{url($photo->url)}}" class="img-fluid img-thumbnail">
                    </div>
                  </form>
                    @endforeach
                        @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
    


        <div class="col-md-9">
  <div class="card card-primary">
      <div class="card-header">
      </div>
    <div class="card-body"> 
      <label>Galería</label>
        <div class="container"><div class="row">
    @if($condo->condophotos->count())
     @foreach($condo->condophotos as $photo)
    <div class="col-3">
      <form method="POST" action="{{route('admin.condophotos.destroy', $photo)}}">
                {{method_field('DELETE')}} {{csrf_field()}}
                <button class="btn btn-danger btn-md" style="position: absolute;"><i class="fa fa-trash"></i></button>
                <img src="{{url($photo->url)}}" class="img-fluid img-thumbnail">
                    </div>
                  </form>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endif
      </div>
 





@endsection


@push('styles')

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- daterange picker -->
  <link rel="stylesheet" href="/adminlte/plugins/daterangepicker/daterangepicker.css">
    <!-- Select2 -->
  <link rel="stylesheet" href="/adminlte/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <link rel="stylesheet" href="/adminlte/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
@endpush

@push('scripts')

<!-- bootstrap color picker -->
 <script src="/adminlte/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.js"></script>
<!-- Select2 -->
<script src="/adminlte/plugins/select2/js/select2.full.min.js"></script>
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<!-- bootstrap datepicker -->
<script src="/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>
<script>

    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2({

      tags:true
    })

    //Initialize Select2 Elements
    $('.select2bs4').select2({

      tags:true
    })
  });
</script>
<script>
  
  var myDropzone = new Dropzone('#dropzone', {


    acceptedFiles: 'image/*',
    paramName: 'photo',
    maxFilesize: 4,
    maxFiles: 1,
    url: '/admin/condos/{{$condo->id}}/condologos',
    headers: {
      'X-CSRF-TOKEN' : '{{csrf_token()}}'
    },
    dictDefaultMessage: 'Da Click o arrastra aquí el logotipo del condominio. Recuerda usar imágenes png con fondo transparente',

  });
  myDropzone.on('error', function(file, res){

    var msg = res.photo[0];
    $('.dz-error-message:last > span').text(msg);
  });
  Dropzone.autoDiscover = false;



  var myDropzone2 = new Dropzone('#dropzone2', {

    acceptedFiles: 'image/*',
    paramName: 'photo',
    maxFilesize: 4,
    maxFiles: 5,
    url: '/admin/condos/{{$condo->id}}/condophotos',
    headers: {
      'X-CSRF-TOKEN' : '{{csrf_token()}}'
    },
    dictDefaultMessage: 'Da Click o arrastra aquí las fotografías del condominio',
    
  });
  myDropzone.on('error', function(file, res){

    var msg = res.photo[0];
    $('.dz-error-message:last > span').text(msg);
  });
  Dropzone.autoDiscover = false;


</script>

<script>
  
    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

        $('.my-colorpicker3').colorpicker()

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
</script>

@endpush

