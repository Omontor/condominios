@extends('admin.layout')
@section('content')

<div class="row">
		
<div class="col-md-7">
	<div class="card card-primary">
    	<div class="card-header"></div>
    	<div class="card-body">   
    	<h3 class="box-title">Datos de Usuario</h3>
    	<hr>
        @include('partials.validationmessages')
    	<form method="POST" action="{{route('admin.users.update', $user)}}">
    		{{csrf_field()}} {{method_field('PUT')}}
    		<div class="form-group">
    			<label for="name">
    				Nombre:
    			</label>
    			<input name="name" value="{{old('name', $user->name)}}" class="form-control">
    		</div>
    		<div class="form-group">
    			<label for="email">
    				email:
    			</label>
    			<input name="email" value="{{old('email', $user->email)}}" class="form-control">
    		</div>

    		    <div class="form-group">
    			<label for="password">
    				Contraseña:
    			</label>
    			<input name="password" class="form-control" type="password" placeholder="Contraseña">
    			    			<span class="help-block"> Dejar en blanco si no quieres cambiar tu contraseña </span>
    		</div>

    		    		    <div class="form-group">
    			<label for="password_confirmation">
    				Confirmar Contraseña:
    			</label>
    			<input name="password_confirmation" class="form-control" type="password" placeholder="Repite la Contraseña">

    		</div>

    		<button class="btn btn-primary btn-block">
    				Actualizar Usuario
    		</button>
    		
    	</form>
    	</div>      

 	</div>
</div>


<div class="col-md-5">
	<div class="card card-primary">
    	<div class="card-header"></div>
    	<div class="card-body">   
    	<h3 class="box-title">Roles</h3>
    	<hr>
        @role('Admin')
    	<form method="POST" action="{{route('admin.users.roles.update', $user)}}">
    		{{csrf_field()}}  {{method_field('PUT')}}
    	@include('admin.roles.checkboxes')
    	<button class="btn btn-primary btn-block"> Actualizar Roles </button>
    	</form>
        @else

        <ul class="list-group">

            @forelse($user->roles as $role)
            <li class="list-group-item">
                {{$role->name}}
            </li>
            @empty
            <li class="list-group-item">No hay roles para mostrar</li>
            @endforelse
        </ul>
        @endrole
    	</div>      

 	</div>

	<div class="card card-primary">
    	<div class="card-header"></div>
    	<div class="card-body">   
    	<h3 class="box-title">Permisos</h3>
    	<hr>
        @role('Admin')
    	<form method="POST" action="{{route('admin.users.permissions.update', $user)}}">
    		{{csrf_field()}}  {{method_field('PUT')}}
    	@include('admin.permissions.checkboxes', ['model' => $user])
    	<button class="btn btn-primary btn-block"> Actualizar Permisos </button>
    	</form>
        @else
                <ul class="list-group">

            @forelse($user->permissions as $permission)
            <li class="list-group-item">
                {{$permission->name}}
            </li>
            @empty
            <li class="list-group-item">No hay permisos para mostrar</li>
            @endforelse
        </ul>
        @endrole
    	</div>      

 	</div>
</div>


</div>
@endsection