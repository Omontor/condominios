@extends('admin.layout')
@section('content')

	<div class="row">
		<div class="col-md-3">
<div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{optional(Auth::user())->avatar}}" alt="{{$user->name}}" width="128">
                </div>

                <h3 class="profile-username text-center">{{$user->name}}</h3>

                <p class="text-muted text-center">{{$user->getRoleNames()->implode(', ')}}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>email</b> <br><a class="float-left">{{$user->email}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Publicaciones</b> <a class="float-right">{{$user->posts->count()}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Roles</b> <a class="float-right">{{$user->getRoleNames()->implode(', ')}}</a>
                  </li>
                </ul>

                <a href="{{route('admin.users.edit', $user)}}" class="btn btn-primary btn-block"><b>Editar</b></a>
              </div>
              <!-- /.card-body -->
            </div>
		</div>
		<div class="col-md-3">
			<div class="card card-primary card-outline">
				 <div class="card-body">
				 	<h3 class="box-title">Publicaciones</h3>
				 </div>
				 <div class="card-body">
				 	
				 	@forelse($user->posts as $post)
  <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
				 	<a href="{{route('posts.show', $post)}}" target="_blank">
				 		<strong> {{$post->title}}</strong>
				 		</a><br>
				 		<small class="text-muted">Publicado el {{$post->published_at->format('d/m/Y')}}</small>
						<p class="text-muted"> {{$post->excerpt}} </p>
						@unless($loop->last)
					
						@endunless
</li>				
	@empty
	<small class="text-muted"> No hay publicaciones</small>
					@endforelse
				 	

				 </div>
			</div>
		</div>
		<div class="col-md-3">
						<div class="card card-primary card-outline">
				 <div class="card-body">
				 	<h3 class="box-title">Permisos</h3>
				 </div>
				 <div class="card-body">
				 	
				 	@forelse($user->roles as $role)
				 			 
				 		<strong> {{$role->name}}</strong>
				 						 		<br>
				 		@if($role->permissions->count())
				 		<small class="text-muted">Permisos: {{$role->permissions->pluck('name')->implode(', ')}}</small>
						@unless($loop->last)
						<hr>
						@endif
						@endunless

					@empty
	<small class="text-muted"> No hay roles asociados</small>
					@endforelse
				 	

				 </div>
			</div>
		</div>
		<div class="col-md-3">
						<div class="card card-primary card-outline">
				 <div class="card-body">
				 	<h3 class="box-title">Permisos Adicionales</h3>
				 </div>
				 <div class="card-body">
				 	
				 	@forelse($user->permissions as $permission)
				 		<strong> {{$permission->name}}</strong>
						@unless($loop->last)
						<hr>
						@endunless

					@empty
	<small class="text-muted"> No hay permisos adicionales asignados</small>
					@endforelse
				 	

				 </div>
			</div>
		</div>
	</div>

@endsection
