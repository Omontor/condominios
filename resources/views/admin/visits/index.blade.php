@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Proveedores de Servicios</h1>
            <small>Listado</small>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Proveedores de Servicios</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')

<div class="row mb-2">       
  <div class="col-sm-3">
    <div class="card card-primary">
      <div class="card-header">
      </div>         
        <div class="card-body">
  <h4>Crear Proveedor Nuevo</h4>

     <div class="form-group">    
      <form method="POST" action="{{route('admin.services.store')}}">
      {{csrf_field()}}
   <label>Nombre</label>
                <input name="name" 
                class="form-control" 
                placeholder="Ingresa el Nombre" 
                value="">
                </input>
            </div>
                            <div class="form-group">                
                <button type="submit" class="btn btn-primary btn-block">
                  Crear Proveedor
                </button>
                </div>
    </form>


        </div>
      </div>
    </div>

<div class="col-sm-9">
    <div class="card card-primary">
      <div class="card-header">
      </div>         
        <div class="card-body">
              <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th> Acciones</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($services as $service)
               <tr>
                 <td>{{$service->id}}</td>
                  <td>{{$service->name}}</td>
                  <td>
                    <a class="btn btn-default btn-sm" href="#" target="_blank"><i class="fas fa-eye"></i></a>
                    <a class="btn btn-info btn-sm" href="#"><i class="fas fa-pencil-alt"></i></a>
                    <form method="POST" action="#" style="display: inline;"> 
                      {{ csrf_field() }} {{method_field('DELETE')}} 
                    <button class="btn btn-danger btn-sm" href="" onclick="return confirm('Estás Seguro de Querer Eliminar Esta Publicación?')"><i class="fas fa-times"></i></button>
                  </form>               
                  </td>

                   </tr>
                  @empty
                  <tr>
                    <td>No hay datos para mostrar</td>
                  </tr>
                  @endforelse
                </tbody>

              </table>
        </div>
      </div>
    </div>
</div>




      
@endsection



@push('styles')
    <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#posts-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>




@endpush