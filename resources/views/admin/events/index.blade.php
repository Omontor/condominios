@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Eventos</h1>
            <small>Listado</small>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Eventos</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')

<div class="row">
    <div class="col-md-5">
<div class="card card-primary card-outline">
<div class="card-body">
  <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Fecha</th>

                  <th>Condominio</th>                              
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($events as $event)
               <tr>
                 <td>{{$event->id}}</td>
                  <td>{{$event->start_date}}</td>

                  <td>
                 {{$event->condo->name}}
                  </td>


     
                  <td>

                    <a class="btn btn-default btn-sm" href="{{route('admin.events.show', $event)}}" target="_blank"><i class="fas fa-eye"></i></a>
                    <a class="btn btn-info btn-sm" href="{{route('admin.events.edit', $event)}}"><i class="fas fa-pencil-alt"></i></a>
                    <form method="POST" action="{{route('admin.events.destroy', $event)}}" style="display: inline;"> 
                      {{ csrf_field() }} {{method_field('DELETE')}}
                      
                    <button class="btn btn-danger btn-sm" href="" onclick="return confirm('Estás Seguro de Querer Eliminar Esta Publicación?')"><i class="fas fa-times"></i></button>

                    </form>
                      

                  </td>

                   </tr>
                  @empty
                  <tr>
                    <td>No hay datos para mostrar</td>
                  </tr>
                  @endforelse
                </tbody>

              </table>
</div>
</div>
    </div>
    <div class="col-md-7">
      <div class="card card-primary card-outline">
         <div class="card-body">
         </div>
         <div class="card-body">
            
            <div id="calendar">
              <br>  
            </div>

         </div>
      </div>
    </div>


  </div>

@endsection



@push('styles')
    <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

  <link href='../packages/core/main.css' rel='stylesheet' />
<link href='../packages/daygrid/main.css' rel='stylesheet' />

<style>


  #calendar {
    max-width: 800px;
    margin: 0 auto;

  }

#calendar .fc-title {

color: white;

}

</style>

@endpush

@push('scripts')
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script src='../packages/core/main.js'></script>
<script src='../packages/interaction/main.js'></script>
<script src='../packages/daygrid/main.js'></script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#posts-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>

  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid' ],
      defaultDate: '   {{ \Carbon\Carbon::now() }}',
      editable: false ,
      eventLimit: true, // allow "more" link when too many events

      events: [

      @forelse($events as $event)
        {
          title: '{{$event->event_name}}',
          start:  '{{$event->start_date}}'
        },
        @empty
        @endforelse
      ]
    });

    calendar.render();
  });

</script>

@endpush