
@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Anunciantes</h1>
            <small>Listado</small>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Anunciantes</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')


<div class="card">
            <div class="card-header" style="vertical-align:  middle;">
              <h3 class="card-title" style="vertical-align:  middle;">Listado de Subcategorías</h3>
            </div>

<div class="card-body">
  <form method="POST" action="{{route('admin.subdivisions.store')}}">
      {{csrf_field()}}


	<h4>Crear Nueva Subcategoría</h4>
	 <label>Nombre</label>
                <input name="name" 
                class="form-control" 
                placeholder="Ingresa el Nombre" 
                value="">
                </input>
                <br>
                <div class="form-group">      
                  <label>Categoría</label>
                  <select name="division_id" class="form-control select2">
                    <option value=""> 
                      Selecciona Una Categoría
                    </option>
                    @forelse($divisions as $division)
                    <option value="{{$division->id}}">{{$division->name}}
                      </option>
                    @empty
                    @endforelse
                  </select>
                </div>



                   <div class="form-group">
                
                <button type="submit" class="btn btn-primary btn-block">
                  Crear Categoría
                </button>
              </div>
               {!!$errors->first('name', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                      </div>

                <div class="form-group {{$errors->has('name') ? 'is-invalid' : ''}}">


</form>
</div>
       
            <!-- /.card-header -->
            <div class="card-body">


              <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Categoría</th>
                  <th> Acciones</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($subdivisions as $subdivision)
               <tr>
                 <td>{{$subdivision->id}}</td>
                  <td>{{$subdivision->name}}</td>
                <td>{{$subdivision->division->name}}</td>
     
                  <td>

                    <a class="btn btn-default btn-sm" href="#" target="_blank"><i class="fas fa-eye"></i></a>
                    <a class="btn btn-info btn-sm" href="#"><i class="fas fa-pencil-alt"></i></a>
                    <form method="POST" action="#" style="display: inline;"> 
                      {{ csrf_field() }} {{method_field('DELETE')}}
                      
                    <button class="btn btn-danger btn-sm" href="" onclick="return confirm('Estás Seguro de Querer Eliminar Esta Publicación?')"><i class="fas fa-times"></i></button>

                    </form>
                      

                  </td>

                   </tr>
                  @empty
                  <tr>
                    <td>No hay datos para mostrar</td>
                  </tr>
                  @endforelse
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>

          
@endsection



@push('styles')
    <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#posts-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>




@endpush