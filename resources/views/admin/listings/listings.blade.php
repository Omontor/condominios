
@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Anunciantes</h1>
            <small>Listado</small>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Anunciantes</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')

 <div class="row mb-2">
<div class="col-sm-4">
  <div class="card card-primary">
      <div class="card-header">
      </div>         
        <div class="card-body">

<form method="POST" action="{{route('admin.listings.store')}}">
      {{csrf_field()}}

  <h4>Crear Nuevo Anunciante</h4>

   <div class="form-group">    
   <label>Nombre</label>
                <input name="title" 
                class="form-control" 
                placeholder="Ingresa el Nombre" 
                value="">
                </input>
            </div>

   <div class="form-group">    
   <label>Dirección</label>
                <input name="address" 
                class="form-control" 
                placeholder="Calle, Número y Colonia" 
                value="">
                </input>
      </div>
            

   <div class="form-group row">
   <div class="col-6">    
   <label>Latitud</label>
                <input name="lat" 
                class="form-control" 
                placeholder="Latitud" 
                value="">
                </input>
              </div>

     <div class="col-6">    
   <label>Longitud</label>
                <input name="lng" 
                class="form-control" 
                placeholder="Longitud" 
                value="">
                </input>
              </div>            
      </div>



                <div class="form-group">      
                  <label>Subategoría</label>
                  <select name="subdivision_id" class="form-control select2">
                    <option value=""> 
                      Selecciona Una Subcategoría
                    </option>
                    @forelse($subdivisions as $subdivision)
                    <option value="{{$subdivision->id}}">{{$subdivision->name}}
                      </option>
                    @empty
                    @endforelse
                  </select>
                </div>





                <div class="form-group">
                <label>Logotipo</label>     
                <input id="image_url" type="file" class="form-control" name="image_url" style="border: none;">                                  
              </div>                        


                <div class="form-group {{$errors->has('description') ? 'is-invalid' : ''}}">
                <label>Descripción</label>
                <br>
                        <textarea id="editor1" name="description" rows="10" cols="65" placeholder="Introduce la descripción del anunciante"></textarea>
                    {!!$errors->first('description', ' <span class="help-block" style= "color:red;">:message</span>')!!}
              </div>

                <br>
                <div class="form-group">                
                <button type="submit" class="btn btn-primary btn-block">
                  Crear Anunciante
                </button>
                </div>
               {!!$errors->first('name', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                      </div>

                <div class="form-group {{$errors->has('name') ? 'is-invalid' : ''}}">


          </form>
        </div>
    </div>
</div>


          <div class="col-sm-8">
<div class="card card-primary">
      <div class="card-header">
      </div>

<div class="card-body">
  



       
            <!-- /.card-header -->
            <div class="card-body">


              <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Logo</th>
                  <th>Nombre</th>
                  <th>Subcategoría</th>
                  <th>Categoría</th>
                  <th> Acciones</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($listings as $listing)
               <tr>
                 <td>{{$listing->id}}</td>
                 <td>
                  <center>
                  <img src="{{$listing->image_url}}" style="max-width: 50px;">                 
                  </center>
   
                </td>
                  <td>{{$listing->title}}</td>
                  <td>{{$listing->subdivision->name}}</td>
                  <td>{{$listing->subdivision->division->name}}</td>
     
                  <td>

                    <a class="btn btn-default btn-sm" href="#" target="_blank"><i class="fas fa-eye"></i></a>
                    <a class="btn btn-info btn-sm" href="#"><i class="fas fa-pencil-alt"></i></a>
                    <form method="POST" action="{{route('admin.listings.destroy', $listing)}}" style="display: inline;"> 
                      {{ csrf_field() }} {{method_field('DELETE')}}
                      
                    <button class="btn btn-danger btn-sm" href="" onclick="return confirm('Estás Seguro de Querer Eliminar Esta Publicación?')"><i class="fas fa-times"></i></button>

                    </form>
                      

                  </td>

                   </tr>
                  @empty
                  <tr>
                    <td>No hay datos para mostrar</td>
                  </tr>
                  @endforelse
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>

</div>
</div>
          
@endsection



@push('styles')
    <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#posts-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": true,
      "autoWidth": false,
    });
  });
</script>




@endpush