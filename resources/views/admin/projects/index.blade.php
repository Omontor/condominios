@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Proyectos</h1>
            <small>Listado</small>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Posts</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')
          <div class="card">
            <div class="card-header" style="vertical-align:  middle;">
              <h3 class="card-title" style="vertical-align:  middle;">Crear Proyecto Nuevo</h3>
              <br>
              <br>
  <form method="POST" action="{{route('admin.projects.store')}}">
      {{csrf_field()}}
                      <div class="form-group {{$errors->has('title') ? 'is-invalid' : ''}}">
                <input id= "post-title" name="title" 
                class="form-control" 
                placeholder="Ingresa el Título de la Publicación" 
                value="{{old('title')}}" autofocus required>
                </input>
               {!!$errors->first('title', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>
                <button  class="btn btn-primary btn-block">Crear Proyecto</button>

</form>
            </div>


            <!-- /.card-header -->
            <div class="card-body">
         <h3 class="card-title" style="vertical-align:  middle;">Listado de Proyectos</h3>
              <br>
              <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Título</th>
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($projects as $project)
               <tr>
                 <td>{{$project->id}}</td>
                  <td>{{$project->title}}</td>
                  <td>

                    <a class="btn btn-default btn-sm" href="#" target="_blank"><i class="fas fa-eye"></i></a>
                    <a class="btn btn-info btn-sm" href="{{route('admin.projects.edit', $project)}}"><i class="fas fa-pencil-alt"></i></a>
                    <form method="POST" action="{{route('admin.projects.destroy', $project)}}" style="display: inline;"> 
                      {{ csrf_field() }} {{method_field('DELETE')}}
                      
                    <button class="btn btn-danger btn-sm" href="" onclick="return confirm('Estás Seguro de Querer Eliminar Esta Publicación?')"><i class="fas fa-times"></i></button>

                    </form>
                      

                  </td>

                   </tr>
                  @empty
                  <tr>
                    <td>No hay datos para mostrar</td>
                  </tr>
                  @endforelse
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
@endsection



@push('styles')
    <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#posts-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>




@endpush