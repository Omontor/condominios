@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Visitantes</h1>
            <small>Listado</small>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Visitantes</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')

<div class="row mb-2">       
  <div class="col-sm-4">
    <div class="card card-primary">
      <div class="card-header">
      </div>         
        <div class="card-body">

<form method="POST" action="{{route('admin.visitors.store')}}">
      {{csrf_field()}}

  <h4>Crear Visitante</h4>



     <div class="form-group">    
   <label>Nombre</label>
                <input name="name" 
                class="form-control" 
                placeholder="Ingresa el Nombre" 
                value="">
                </input>
            </div>

                <div class="form-group">      
                  <label>Vivienda que visitará</label>
                  <select name="property_id" class="form-control select2">
                    <option value=""> 
                      Selecciona Una Vivienda
                    </option>
                    @forelse($properties as $property)
                    <option value="{{$property->id}}">{{$property->address}}
                      </option>
                    @empty
                    @endforelse
                  </select>
                </div>
                  


                  <label>Próxima visita:</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input name="next_visit" 
                    type="text" 
                    class="form-control float-right" 
                    id="datepicker" 
                    value="{{Carbon\Carbon::now()->format('m/d/Y')}}">
                  </div>
                  <!-- /.input group -->
             



                <br>
                <div class="form-group">                
                <button type="submit" class="btn btn-primary btn-block">
                  Crear Visitante
                </button>
                </div>
               {!!$errors->first('name', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                      </div>

                <div class="form-group {{$errors->has('name') ? 'is-invalid' : ''}}">
                </div>

          </form>

          </div>
      </div>


<div class="col-sm-8">
    <div class="card card-primary">
      <div class="card-header">
      </div>         
          <div class="card-body">
              <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Vivienda</th>
                  <th>Condominio</th>                              
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($visitors as $visitor)
               <tr>
                 <td>{{$visitor->id}}</td>
                  <td>{{$visitor->name}}</td>
                  <td>
                    {{ $visitor->property->address }}
                  </td>

                  <td>{{ $visitor->property->condo->name }}</td>
     
                  <td>

                    <a class="btn btn-default btn-sm" href="{{route('admin.visitors.show', $visitor)}}" target="_blank"><i class="fas fa-eye"></i></a>
                    <a class="btn btn-info btn-sm" href="{{route('admin.visitors.edit', $visitor)}}"><i class="fas fa-pencil-alt"></i></a>
                    <form method="POST" action="{{route('admin.visitors.destroy', $visitor)}}" style="display: inline;"> 
                      {{ csrf_field() }} {{method_field('DELETE')}}
                      
                    <button class="btn btn-danger btn-sm" href="" onclick="return confirm('Estás Seguro de Querer Eliminar Esta Publicación?')"><i class="fas fa-times"></i></button>

                    </form>
                      

                  </td>

                   </tr>
                  @empty
                  <tr>
                    <td>No hay datos para mostrar</td>
                  </tr>
                  @endforelse
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
@endsection


@push('styles')
    <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- daterange picker -->
  <link rel="stylesheet" href="/adminlte/plugins/daterangepicker/daterangepicker.css">
    <!-- Select2 -->
  <link rel="stylesheet" href="/adminlte/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endpush

@push('scripts')

<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.js"></script>
<!-- Select2 -->
<script src="/adminlte/plugins/select2/js/select2.full.min.js"></script>
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<!-- bootstrap datepicker -->
<script src="/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#posts-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    CKEDITOR.config.height = 355;
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
<script>

    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2({

      tags:true
    })

    //Initialize Select2 Elements
    $('.select2bs4').select2({

      tags:true
    })
  });
</script>

@endpush
