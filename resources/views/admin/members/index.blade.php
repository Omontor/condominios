@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Habitantes</h1>
            <small>Listado</small>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Habitantes</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')

<div class="row mb-2">       
  <div class="col-sm-4">
    <div class="card card-primary">
      <div class="card-header">
      </div>         
        <div class="card-body">

<form method="POST" action="{{route('admin.members.store')}}">
      {{csrf_field()}}

  <h4>Asociar Habitante</h4>



  

                <div class="form-group">      
                  <label>Nombre del Usuario</label>
                  <select name="user_id" class="form-control select2">
                    <option value=""> 
                      Selecciona Un Usuario
                    </option>
                    @forelse($users as $user)
                    <option value="{{$user->id}}">{{$user->name}}
                      </option>
                    @empty
                    @endforelse
                  </select>
                </div>
  

                <div class="form-group">      
                  <label>Propiedades</label>
                  <select name="property_id" class="form-control select2">
                    <option value=""> 
                      Selecciona Una Propiedad
                    </option>
                    @forelse($properties as $property)
                    <option value="{{$property->id}}">{{$property->address}}
                      </option>
                    @empty
                    @endforelse
                  </select>
                </div>




                <br>
                <div class="form-group">                
                <button type="submit" class="btn btn-primary btn-block">
                  Crear Anunciante
                </button>
                </div>
               {!!$errors->first('name', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                      </div>

                <div class="form-group {{$errors->has('name') ? 'is-invalid' : ''}}">


          </form>

          </div>
      </div>
    </div>

<div class="col-sm-8">
    <div class="card card-primary">
      <div class="card-header">
      </div>         
          <div class="card-body">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Vivienda</th>          
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($members as $member)
               <tr>
                 <td>{{$member->id}}</td>
                  <td>{{$member->user->name}}</td>
                  <td>
                    {{ $member->property->address }}
                  </td>
     
                  <td>

                    <a class="btn btn-default btn-sm" href="{{route('admin.members.show', $member)}}" target="_blank"><i class="fas fa-eye"></i></a>
                    <a class="btn btn-info btn-sm" href="{{route('admin.members.edit', $member)}}"><i class="fas fa-pencil-alt"></i></a>
                    <form method="POST" action="{{route('admin.members.destroy', $member)}}" style="display: inline;"> 
                      {{ csrf_field() }} {{method_field('DELETE')}}
                      
                    <button class="btn btn-danger btn-sm" href="" onclick="return confirm('Estás Seguro de Querer Eliminar Esta Publicación?')"><i class="fas fa-times"></i></button>

                    </form>
                      

                  </td>

                   </tr>
                  @empty
                  <tr>
                    <td>No hay datos para mostrar</td>
                  </tr>
                  @endforelse
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
    
@endsection



@push('styles')
    <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#posts-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>




@endpush