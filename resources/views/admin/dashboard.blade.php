@extends('admin.layout')
@section('header')
@stop
@section('content')
<section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->

 <div class="card">
            <!-- /.card-header -->
            <div class="card-body">



        <div class="row">
<div class="col-lg-8">
 
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    var markers = [

                @forelse($condos as $condo)

    {     
        "title": '{{$condo->name}}}',
        "lat": '{{$condo->lat}}',
        "lng": '{{$condo->lng}}',
        "description": '{{$condo->name}}'
    },
    @empty
    @endforelse
    
    ];
    window.onload = function () {
        LoadMap();
    }
    function LoadMap() {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            // zoom: 8, //Not required.
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
 
        //Create LatLngBounds object.
        var latlngbounds = new google.maps.LatLngBounds();
 
        for (var i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);
 
            //Extend each marker's position in LatLngBounds object.
            latlngbounds.extend(marker.position);
        }
 
        //Get the boundaries of the Map.
        var bounds = new google.maps.LatLngBounds();
 
        //Center map and adjust Zoom based on the position of all markers.
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
    }
</script>
<div id="dvMap" style="width: 100%; height: 620px">
</div>


<script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-VZUr01EdXUPZfUcj_UilKvo1DjmfGG0&callback=initMap">
                        </script>
                        <br><br>


</div>
 <div class="col-lg-4">
<div class="info-box bg-gray">

            <span class="info-box-icon"><i class="fas fa-city"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Condominios</span>
              <span class="info-box-number">{{$condoscount}}</span>

              <div class="progress">
                <div class="progress-bar" style="width: 50%"></div>
              </div>
              <span class="progress-description">
                    50% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>

          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fas fa-home"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Viviendas</span>
              <span class="info-box-number">{{$propertiescount}}</span>

              <div class="progress">
                <div class="progress-bar" style="width: 20%"></div>
              </div>
              <span class="progress-description">
                    20% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>

          <div class="info-box bg-black">
            <span class="info-box-icon"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Habitantes</span>
              <span class="info-box-number">{{$memberscount}}</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
              <span class="progress-description">
                    70% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>

          <div class="info-box bg-blue">
            <span class="info-box-icon"><i class="fas fa-boxes"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Proveedores</span>
              <span class="info-box-number">{{$listingscount}}</span>

              <div class="progress">
                <div class="progress-bar" style="width: 40%"></div>
              </div>
              <span class="progress-description">
                    40% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>

          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fas fa-id-badge"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Visitantes</span>
              <span class="info-box-number">{{$visitorscount}}</span>

              <div class="progress">
                <div class="progress-bar" style="width: 40%"></div>
              </div>
              <span class="progress-description">
                    40% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          
</div>

<br>
<div class="col-lg-3">
<div class="small-box bg-blue">
            <div class="inner">
              <h3>${{$expected_income}}</h3>

              <p>Ingresos Esperados del mes</p>
            </div>
            <div class="icon">
              <i class="fas fa-hand-holding-usd"></i>
            </div>
            <a href="#" class="small-box-footer">
             Generar Excel <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
</div>
<div class="col-lg-3">
<div class="small-box bg-green">
            <div class="inner">
              <h3>${{$paymentscount}}</h3>

              <p>Ingresos Recibidos</p>
            </div>
            <div class="icon">
              <i class="fas fa-money-bill-alt"></i>
            </div>
            <a href="#" class="small-box-footer">
             Generar Excel <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
</div>

<div class="col-lg-3">
<div class="small-box bg-black">
            <div class="inner">
              <h3>{{$debtscount}}</h3>

              <p>Viviendas con adeudo</p>
            </div>
            <div class="icon">
              <i class="fas fa-money-bill-alt"></i>
            </div>
            <a href="#" class="small-box-footer">
             Ver Todas <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
</div>

<div class="col-lg-3">
<div class="small-box bg-red">
            <div class="inner">
              <h3>${{$debts}}</h3>

              <p>Adeudo Total</p>
            </div>
            <div class="icon">
              <i class="fas fa-money-bill-alt"></i>
            </div>
            <a href="#" class="small-box-footer">
             Generar Excel <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
</div>



<div class="col-lg-6">
<div class="small-box bg-info">
            <div class="inner">
              <h3>{{$divisionscount}}</h3>

              <p>Categorías de proveedores</p>
            </div>
            <div class="icon">
              <i class="fas fa-boxes"></i>
            </div>
            <a href="#" class="small-box-footer">
             Generar Excel <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
</div>

<div class="col-lg-6">
<div class="small-box bg-secondary">
            <div class="inner">
              <h3>{{$subdivisionscount}}</h3>

              <p>Subcategorías de proveedores</p>
            </div>
            <div class="icon">
              <i class="fas fa-boxes"></i>
            </div>
            <a href="#" class="small-box-footer">
             Generar Excel <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
</div>


<div class="col-12">
  <h2>Últimos Condominios Registrados</h2>
<br>
         <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Logo</th>
                  <th>Nombre</th>
                  <th>Dirección</th>
  
                </tr>
                </thead>
                <tbody>
                  @forelse($condos as $condo)
               <tr>
                 <td>{{$condo->id}}</td>
                  <td> <center><img src="{{$condo->logo_url}}" width="50px"></center></td>
                  <td>{{$condo->name}}</td>
                  <td>{{$condo->address}}</td>

                   </tr>
                  @empty
                  <tr>
                    <td>No hay datos para mostrar</td>
                  </tr>
                  @endforelse
                </tbody>

              </table>
</div>

<div class="col-12">
<br>
<br>
</div>

<div class="col-lg-7">
  <h2>Últimas Viviendas</h2>
      <br>  
         <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>

                  <th>Dirección</th>
                  <th>Condominio</th>
                  <th>Habitantes</th>
                  
                </tr>
                </thead>
                <tbody>
                  @forelse($properties as $property)
               <tr>
                 <td>{{$property->id}}</td>

                  <td>{{$property->address}}</td>
                  <td>{{$property->condo->name}}</td>
                  <td><center>
                    {{count($property->member)}}
                    </center>
                  </td>
                   </tr>
                  @empty
                  <tr>
                    <td>No hay datos para mostrar</td>
                  </tr>
                  @endforelse
                </tbody>

              </table>
</div>

<div class="col-lg-5">
    <h2>Últimos Mensajes</h2>
    <br>
 <div class="box box-default">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="callout callout-danger">
                <h4>I am a danger callout!</h4>

                <p>There is a problem that we need to fix. A wonderful serenity has taken possession of my entire soul,
                  like these sweet mornings of spring which I enjoy with my whole heart.</p>
              </div>
              <div class="callout callout-info">
                <h4>I am an info callout!</h4>

                <p>Follow the steps to continue to payment.</p>
              </div>
              <div class="callout callout-warning">
                <h4>I am a warning callout!</h4>

                <p>This is a yellow callout.</p>
              </div>
              <div class="callout callout-success">
                <h4>I am a success callout!</h4>

                <p>This is a green callout.</p>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
</div>



  <div class="col-lg-6">
  <h2>Últimos Proveedores</h2>
       <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Logo</th>
                  <th>Nombre</th>
                  <th>Dirección</th>
                  <th>Subcategoría</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($listings as $listing)
               <tr>
                 <td>{{$listing->id}}</td>
                  <td> <center><img src="{{$listing->image_url}}" width="50px"></center></td>
                  <td>{{$listing->title}}</td>
                  <td>{{$listing->address}}</td>
                  <td>
                    {{$listing->subdivision->name}}
                  </td>

                   </tr>
                  @empty
                  <tr>
                    <td>No hay datos para mostrar</td>
                  </tr>
                  @endforelse
                </tbody>

              </table>  
  </div>

<div class="col-lg-6">
    <h2>Últimos Incidentes</h2>
    <br>
  <div class="container my-4">


    <!--Carousel Wrapper-->
    <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
      <!--Indicators-->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-2" data-slide-to="1"></li>
        <li data-target="#carousel-example-2" data-slide-to="2"></li>
      </ol>
      <!--/.Indicators-->
      <!--Slides-->
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
          <div class="view">
            <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg" alt="First slide">
            <div class="mask rgba-black-light"></div>
          </div>
          <div class="carousel-caption">
            <h3 class="h3-responsive">Light mask</h3>
            <p>First text</p>
          </div>
        </div>
        <div class="carousel-item">
          <!--Mask color-->
          <div class="view">
            <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(6).jpg" alt="Second slide">
            <div class="mask rgba-black-strong"></div>
          </div>
          <div class="carousel-caption">
            <h3 class="h3-responsive">Strong mask</h3>
            <p>Secondary text</p>
          </div>
        </div>
        <div class="carousel-item">
          <!--Mask color-->
          <div class="view">
            <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(9).jpg" alt="Third slide">
            <div class="mask rgba-black-slight"></div>
          </div>
          <div class="carousel-caption">
            <h3 class="h3-responsive">Slight mask</h3>
            <p>Third text</p>
          </div>
        </div>
      </div>
      <!--/.Slides-->
      <!--Controls-->
      <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->

  </div>
</div>
<hr>
<div class="col-12">
                <h2>Notificación Masiva</h2>
<div class="small-box bg-blue">
            <div class="inner">

<br>
           <form>
  <div class="form-group">
    <label for="exampleInputEmail1">Título</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Contenido</label>
    <input type="text" class="form-control" id="exampleInputPassword1">
  </div>
  <center>
  <button type="submit" class="btn btn-light btn-lg">Enviar</button>
  </center>
  <br>
</form>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
          
          </div>
</div>

<div class="col-12">
    <h2>Sitio Web</h2>
</div>

          <div class="col-lg-4 col-12">

            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$users}}</h3>
                <p>Usuarios</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="{{route('admin.users.index')}}" class="small-box-footer">Ver más <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$posts}}</h3>
                <p>Publicaciones</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="{{route('admin.posts.index')}}" class="small-box-footer">Ver Más <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$categories}}</h3>

                <p>Categorías</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
                    <div class="col-lg-6 col-12">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3>{{$projectcategories}}</h3>

                <p>Categorías de Proyectos</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
                    <div class="col-lg-6 col-12">
            <!-- small box -->
            <div class="small-box bg-secondary">
              <div class="inner">
                <h3>{{$projects}}</h3>

                <p>Proyectos</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
@endsection