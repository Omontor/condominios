<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <form method="POST" action="{{route('admin.posts.store', '#create')}}">
  {{csrf_field()}}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Agrega el título de tu nueva publicación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <div class="form-group {{$errors->has('title') ? 'is-invalid' : ''}}">
                <input id= "post-title" name="title" 
                class="form-control" 
                placeholder="Ingresa el Título de la Publicación" 
                value="{{old('title')}}" autofocus required>
                </input>
               {!!$errors->first('title', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button  class="btn btn-primary">Crear Publicación</button>
      </div>
    </div>
  </div>
</form>
</div>

@push('scripts')
<script>
  
  if (window.location.hash === '#create')
  {

    $('#exampleModalLong').modal('show');


  }

      $('#exampleModalLong').on('hide.bs.modal', function(){

        window.location.hash = '#';

      });

            $('#exampleModalLong').on('shown.bs.modal', function(){

        $('#post-title').focus();
        window.location.hash = '#create';

      });

</script>
@endpush