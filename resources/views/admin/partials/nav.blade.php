  <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
<li class="header" style="color: white;">Administración</li>
<br>
            <li class="nav-item">
            <a href="{{route('dashboard')}}" class= "nav-link {{setActiveRoute('dashboard')}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Panel de Control
              </p>
            </a>
          </li>
<!-- Inicia Condos-->          
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.condos.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-city"></i>
              <p>
                Condominios
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.condos.index')}}" class="nav-link {{setActiveRoute('admin.condos.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todos los Condominios</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong2" class="nav-link">
                  <i class="fas fa-pencil-alt nav-icon"></i>
                  <p>Crear Condominio</p>
                </a>
              </li>
            </ul>
          </li>
<!-- Termina Condos-->   


<!-- Inicia Properties-->          
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.properties.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Viviendas
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.properties.index')}}" class="nav-link {{setActiveRoute('admin.properties.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todas las Viviendas</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong" class="nav-link">
                  <i class="fas fa-pencil-alt nav-icon"></i>
                  <p> Cargar excel de Viviendas</p>
                </a>
              </li>
            </ul>
          </li>
<!-- Termina Properties--> 




<!-- Inicia Properties-->          
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.debts.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-file-invoice"></i>
              <p>
                Adeudos
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.debts.index')}}" class="nav-link {{setActiveRoute('admin.debts.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todos los Adeudos</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong" class="nav-link">
                  <i class="fas fa-table nav-icon"></i>
                  <p> Exportar Excel de Adeudos</p>
                </a>
              </li>
            </ul>
          </li>

<!-- Termina Properties--> 


<!-- Inicia Properties-->          
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.payments.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-money-bill"></i>
              <p>
                Pagos
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.payments.index')}}" class="nav-link {{setActiveRoute('admin.payments.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todos los Pagos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong" class="nav-link">
                  <i class="fas fa-table nav-icon"></i>
                  <p> Exportar Excel de Pagos</p>
                </a>
              </li>
            </ul>
          </li>

<!-- Termina Properties--> 


<!-- Inicia Properties-->          
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.listings.*')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-boxes"></i>
              <p>
                Anunciantes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.listings.categories')}}" class="nav-link {{setActiveRoute('admin.listings.categories')}}">
                  <i class="fas fa-sitemap nav-icon"></i>
                  <p>Categorías</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.listings.subcategories')}}" class="nav-link {{setActiveRoute('admin.listings.subcategories')}}" class="nav-link">
                  <i class="fas fa-ellipsis-v nav-icon"></i>
                  <p> Subcategorías</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{route('admin.listings.listings')}}" class="nav-link {{setActiveRoute('admin.listings.listings')}}" class="nav-link">
                  <i class="fas fa-list-ul nav-icon"></i>
                  <p> Listado</p>
                </a>
              </li>
            </ul>
          </li>

<!-- Termina Properties--> 


<!-- Inicia Properties-->          
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.members.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Habitantes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.members.index')}}" class="nav-link {{setActiveRoute('admin.members.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todos los Habitantes</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong" class="nav-link">
                  <i class="fas fa-table nav-icon"></i>
                  <p> Cargar Masiva</p>
                </a>
              </li>
            </ul>
          </li>

<!-- Termina Properties--> 
          <hr>
<!-- Inicia Properties-->          
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.visitors.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Visitantes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.visitors.index')}}" class="nav-link {{setActiveRoute('admin.visitors.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todos los Visitantes</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong" class="nav-link">
                  <i class="fas fa-table nav-icon"></i>
                  <p> Exportar excel de Visitantes</p>
                </a>
              </li>
            </ul>
          </li>

<!-- Termina Properties--> 


<!-- Inicia Properties-->          
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.visits.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-concierge-bell"></i>
              <p>
                Proveedores
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.visits.index')}}" class="nav-link {{setActiveRoute('admin.visits.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todos</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong" class="nav-link">
                  <i class="fas fa-table nav-icon"></i>
                  <p> Exportar excel de Visitas</p>
                </a>
              </li>
            </ul>
          </li>

<!-- Termina Properties--> 

<!-- Inicia Properties-->          
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.commonareas.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-window-restore"></i>
              <p>
                Áreas Comunes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.commonareas.index')}}" class="nav-link {{setActiveRoute('admin.commonareas.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todas</p>
                </a>
              </li>
            </ul>
          </li>
<!-- Termina Properties-->    


<!-- Inicia Properties-->          
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.reservations.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Reservaciones
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.reservations.index')}}" class="nav-link {{setActiveRoute('admin.reservations.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todas</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong" class="nav-link">
                  <i class="fas fa-plus nav-icon"></i>
                  <p> Crear</p>
                </a>
              </li>

            </ul>
          </li>
<!-- Termina Properties-->   

<!-- Inicia Properties-->          
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.events.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-calendar"></i>
              <p>
                Eventos
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.events.index')}}" class="nav-link {{setActiveRoute('admin.events.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todas</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong" class="nav-link">
                  <i class="fas fa-plus nav-icon"></i>
                  <p> Crear</p>
                </a>
              </li>

            </ul>
          </li>
<!-- Termina Properties-->   

<!-- Inicia Properties-->          
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.incidents.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-exclamation-circle"></i>
              <p>
                Incidentes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.incidents.index')}}" class="nav-link {{setActiveRoute('admin.incidents.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todas</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong" class="nav-link">
                  <i class="fas fa-plus nav-icon"></i>
                  <p> Crear</p>
                </a>
              </li>

            </ul>
          </li>
<!-- Termina Properties-->   

<hr>
<li class="header" style="color: white;">Sitio Web</li>
<br>
<!-- Inicia Blog-->          
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.posts.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-newspaper"></i>
              <p>
                Blog
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.posts.index')}}" class="nav-link {{setActiveRoute('admin.posts.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todos los Post</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong" class="nav-link">
                  <i class="fas fa-pencil-alt nav-icon"></i>
                  <p>Crear Post</p>
                </a>
              </li>
            </ul>
          </li>
<!-- Termina Blog-->    
<!-- Inicia Portfolio-->
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.projects.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Proyectos
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.projects.index')}}" class="nav-link {{setActiveRoute('admin.projects.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todos los Proyectos</p>
                </a>
              </li>
            </ul>
          </li>

<!-- Termina Portfolio-->

<!-- Inicia Páginas Estáticas-->
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.page.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Páginas
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.page.index')}}" class="nav-link {{setActiveRoute('admin.page.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todas las páginas</p>
                </a>
              </li>

            </ul>
          </li>

<!-- Termina Páginas Estáticas-->   

<!-- Inicia Medios -->


          <li class="nav-item has-treeview  {{setActiveTreeview('admin.media.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-image"></i>
              <p>
                Imágenes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.media.index')}}" class="nav-link {{setActiveRoute('admin.media.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todas las imágenes</p>
                </a>
              </li>

            </ul>
          </li>


<!-- Termina Medios -->

<!-- Inicia Contacto-->
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.contacts.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-address-card"></i>
              <p>
                Contacto
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.contacts.index')}}" class="nav-link {{setActiveRoute('admin.contacts.index')}}">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todas los registros</p>
                </a>
              </li>
            </ul>
          </li>

<!-- Termina Contacto-->   





          @can('view', new App\User)
          <li class="nav-item has-treeview  {{setActiveTreeview(['admin.users.index', 'admin.users.create'])}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Usuarios
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.users.index')}}" class="nav-link {{setActiveRoute('admin.users.index')}}">
                  <i class="far fa-user nav-icon"></i>
                  <p>Ver Todos los Usuarios</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.users.create')}}" class="nav-link {{setActiveRoute('admin.users.create')}}">
                  <i class="fas fa-pencil-alt nav-icon"></i>
                  Crear Usuario
                </a>
              </li>
            </ul>
          </li>
          @else
          <li class="nav-item">
            <a href="{{route('admin.users.show', auth()->user())}}" class= "nav-link {{setActiveRoute([ 'admin.users.show', 'admin.users.edit'])}}">
              <i class="nav-icon fas fa-user-alt"></i>
              <p>
                Perfil
              </p>
            </a>
          </li>
          @endcan


<!-- Inicia Settings-->
 @can('view', new \Spatie\Permission\Models\Permission)
          <li class="nav-item has-treeview  {{setActiveTreeview('admin.setting.index')}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Configuración
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

            <li class="nav-item">
            <a href="{{route('admin.setting.index')}}" class= "nav-link {{setActiveRoute(['admin.setting.index'])}}">
              <i class="nav-icon fas fa-pencil-alt"></i>
              <p>
                Configuración de Sitio
              </p>
            </a>
          </li>
         
            <li class="nav-item">
            <a href="{{route('admin.roles.index')}}" class= "nav-link {{setActiveRoute(['admin.roles.index', 'admin.roles.edit'])}}">
              <i class="nav-icon fas fa-pencil-alt"></i>
              <p>
                Roles
              </p>
            </a>
          </li>
          @endcan
          @can('view', new \Spatie\Permission\Models\Permission)
            <li class="nav-item">
            <a href="{{route('admin.permissions.index')}}" class= "nav-link {{setActiveRoute(['admin.permissions.index', 'admin.permissions.edit'])}}">
              <i class="nav-icon fas fa-pencil-alt"></i>
              <p>
                Permisos
              </p>
            </a>
          </li>
          @endcan
 
            </ul>
          </li>

<!-- Termina Settings-->   



        </ul>
      </nav>
