@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Editar Vivienda</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item"><a href="{{route('admin.properties.index')}}">Viviendas</a></li>
              <li class="breadcrumb-item active">Crear</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')

<form method="POST" action="{{route('admin.properties.update', $property)}}">
  {{csrf_field()}} {{method_field('PUT')}}
 <div class="row mb-2">
<div class="col-md-8">
        <div class="card card-primary">
            <div class="card-header">
            </div>         
        <div class="card-body">
                      
              <div class="form-group {{$errors->has('address') ? 'is-invalid' : ''}}">
                <label>Dirección</label>
                <input id= "address" name="address" 
                class="form-control" 
                placeholder="Ingresa la dirección de la vivienda" 
                value="{{old('address',$property->address)}}" autofocus required>
                </input>
               {!!$errors->first('address', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>

                <div class="form-group {{$errors->has('condo_id') ? 'is-invalid' : ''}}">      
                  <label>Condominio</label>
                  <select name="condo_id" class="form-control select2">
                    <option value=""> 
                      Selecciona Un Condominio
                    </option>
                    @forelse($condos as $condo)
                    <option value="{{$condo->id}}"  {{old('condo_id', $property->condo_id) == $condo->id ? 'selected' : '' }}
                      >{{$condo->name}}
                      </option>
                    @empty
                    @endforelse
                  </select>
                </div>

                <div class="form-group {{$errors->has('area') ? 'is-invalid' : ''}}">
                <label>Área</label>
                <input id= "area" name="area" 
                class="form-control" 
                placeholder="Ingresa el área vivienda" 
                value="{{old('area', $property->area)}}" autofocus required>
                </input>
               {!!$errors->first('area', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>

                <div class="form-group {{$errors->has('members') ? 'is-invalid' : ''}}">
                <label>Habitantes</label>
                <input id= "members" name="members" 
                class="form-control" 
                placeholder="Número de habitantes de la vivienda" 
                value="{{old('members', $property->members)}}" autofocus required>
                </input>
               {!!$errors->first('members', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>  
                <div class="form-group">                 
                <button type="submit" class="btn btn-primary btn-block">
                    Actualizar Vivienda
                </button>
                </div>
              </form>   
            </div>
          </div>
        </div>
              <div class="col-md-4">
                <div class="card card-primary">
                  <div class="card-header">
                  </div>         
              <div class="card-body">
                <div class="col-12">
                  <center> 
                 <h3> <img src="{{$currentcondo->first()->logo_url}}" style="max-width: 100px;" class="img-circle shadow"></h3>
                 <br>
                 </center>
               </div>
               <div class="col-12">
                 <h3> {{$currentcondo->first()->name}}</h3>
               </div>
               <hr>
                <div class="col-12">
                 <p> {{$currentcondo->first()->address}}</p>
                 <p>Mantenimiento Mensual: ${{$currentcondo->first()->monthly_maintenance}}</p>
               </div>

               <div class="col-12">
                 <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    var markers = [



    {     
        "title": '{{$currentcondo->first()->name}}}',
        "lat": '{{$currentcondo->first()->lat}}',
        "lng": '{{$currentcondo->first()->lng}}',
        "description": '{{$currentcondo->first()->name}}'
    },

    
    ];
    window.onload = function () {
        LoadMap();
    }
    function LoadMap() {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
             zoom: 18, //Not required.
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
 
 
        for (var i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);
 
            //Extend each marker's position in LatLngBounds object.
            latlngbounds.extend(marker.position);
        }
 
        //Get the boundaries of the Map.
        var bounds = new google.maps.LatLngBounds();
 
        //Center map and adjust Zoom based on the position of all markers.
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
    }
</script>
<div id="dvMap" style="width: 100%; height: 400px">
</div>


<script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-VZUr01EdXUPZfUcj_UilKvo1DjmfGG0&callback=initMap">
                        </script>s
               </div>
              </div>
      </div>





@endsection


@push('styles')

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- daterange picker -->
  <link rel="stylesheet" href="/adminlte/plugins/daterangepicker/daterangepicker.css">
    <!-- Select2 -->
  <link rel="stylesheet" href="/adminlte/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endpush

@push('scripts')


<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.js"></script>
<!-- Select2 -->
<script src="/adminlte/plugins/select2/js/select2.full.min.js"></script>
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<!-- bootstrap datepicker -->
<script src="/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    CKEDITOR.config.height = 355;
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
<script>

    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2({

      tags:true
    })

    //Initialize Select2 Elements
    $('.select2bs4').select2({

      tags:true
    })
  });
</script>
@endpush

