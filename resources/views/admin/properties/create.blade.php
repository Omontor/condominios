<!-- Modal -->
<div class="modal fade" id="exampleModalLong3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <form method="POST" action="{{route('admin.properties.store', '#create')}}">
  {{csrf_field()}}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Crear Vivienda Nueva</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <div class="form-group {{$errors->has('title') ? 'is-invalid' : ''}}">
                <label>Dirección</label>
                <input id= "address" name="address" 
                class="form-control" 
                placeholder="Ingresa la dirección de la vivienda" 
                value="{{old('title')}}" autofocus required>
                </input>
               {!!$errors->first('title', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>


                <div class="form-group {{$errors->has('condo_id') ? 'is-invalid' : ''}}">      
                  <label>Condominio</label>
                  <select name="condo_id" class="form-control select2">
                    <option value=""> 
                      Selecciona Un Condominio
                    </option>
                    @forelse($condos as $condo)
                    <option value="{{$condo->id}}">{{$condo->name}}
                      </option>
                    @empty
                    @endforelse
                  </select>
                </div>

                <div class="form-group {{$errors->has('title') ? 'is-invalid' : ''}}">
                <label>Área</label>
                <input id= "area" name="area" 
                class="form-control" 
                placeholder="Ingresa el área vivienda" 
                value="{{old('title')}}" autofocus required>
                </input>
               {!!$errors->first('title', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>

                <div class="form-group {{$errors->has('title') ? 'is-invalid' : ''}}">
                <label>Habitantes</label>
                <input id= "members" name="members" 
                class="form-control" 
                placeholder="Número de habitantes de la vivienda" 
                value="{{old('title')}}" autofocus required>
                </input>
               {!!$errors->first('title', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>

                
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button  class="btn btn-primary">Crear Vivienda</button>
      </div>
    </div>
  </div>
</form>
</div>

@push('styles')
  <link rel="stylesheet" href="/adminlte/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endpush

@push('scripts')

<!-- Select2 -->
<script src="/adminlte/plugins/select2/js/select2.full.min.js"></script>
<script>
  
  if (window.location.hash === '#create')
  {

    $('#exampleModalLong3').modal('show');


  }

      $('#exampleModalLong3').on('hide.bs.modal', function(){

        window.location.hash = '#';

      });

            $('#exampleModalLong3').on('shown.bs.modal', function(){

        $('#post-title').focus();
        window.location.hash = '#create';

      });

</script>

@endpush