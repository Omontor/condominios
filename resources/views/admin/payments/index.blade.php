@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pagos</h1>
            <small>Listado</small>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Pagos</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')
          <div class="card">
            <div class="card-header" style="vertical-align:  middle;">
              <h3 class="card-title" style="vertical-align:  middle;">Listado de Pagos</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Vivienda</th>
                  <th>Fecha</th>
                  <th>Cantidad Recibida</th>            
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($payments as $payment)
               <tr>
                 <td>{{$payment->id}}</td>
                  <td>{{$payment->property->address}}</td>
                  <td><center>
                    {{ \Carbon\Carbon::parse($payment->payment_date)->format('d, M, Y') }}
                </center>	
                  </td>
                   <td><center>$	{{$payment->payment_amount}}</center></td>
                  <td>

                    <a class="btn btn-default btn-sm" href="{{route('admin.payments.show', $payment)}}" target="_blank"><i class="fas fa-eye"></i></a>
                    <a class="btn btn-info btn-sm" href="{{route('admin.payments.edit', $payment)}}"><i class="fas fa-pencil-alt"></i></a>
                    <form method="POST" action="{{route('admin.payments.destroy', $payment)}}" style="display: inline;"> 
                      {{ csrf_field() }} {{method_field('DELETE')}}
                      
                    <button class="btn btn-danger btn-sm" href="" onclick="return confirm('Estás Seguro de Querer Eliminar Esta Publicación?')"><i class="fas fa-times"></i></button>

                    </form>
                      

                  </td>

                   </tr>
                  @empty
                  <tr>
                    <td>No hay datos para mostrar</td>
                  </tr>
                  @endforelse
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
@endsection



@push('styles')
    <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#posts-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      
    });
  });
</script>




@endpush