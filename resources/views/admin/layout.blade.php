<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>{{config('app.name')}}</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/adminlte/plugins/fontawesome-free/css/all.min.css">
  @stack('styles')

  <!-- Theme style -->
  <link rel="stylesheet" href="/adminlte/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/" class="nav-link">Inicio</a>
      </li>

    </ul>

    <!-- SEARCH FORM -->


    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li><div class="image" style="margin-right: 10px;">
          <img src="{{optional(Auth::user())->avatar}}" class="img-circle elevation-2" alt="{{auth()->user()->name}}" width="40">
        </div></li>
      <!-- esta es el área para agregar widgets al topbar -->
      <li>
      <div class="dropdown show" style="margin-right: 50px;">
  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{auth()->user()->name}}
  </a>

  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item" href="#">{{auth()->user()->getRoleDisplayNames()}} </a>
    <a class="dropdown-item" href="#"><small>Miembro desde <br> {{auth()->user()->created_at->format('d/m/Y')}}</a></small>
    <div class="dropdown-item">
      <form method="POST" action="{{route('logout')}}">
        {{csrf_field()}}
      <button class="btn btn-danger btn-block"
      >Cerrar Sesión</button>
      </form>
  </div>
</div>
</li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/admin" class="brand-link">

      <span class="brand-text font-weight-light">{{config('app.name')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar" >
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex" style="vertical-align: middle;">
        <div class="image">
          <img src="{{optional(Auth::user())->avatar}}" class="img-circle elevation-2" alt="{{auth()->user()->name}}">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{auth()->user()->name}} <br> <small>{{optional(auth()->user()->roles()->first())->name}} </small></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
   @include('admin.partials.nav')
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>



<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        @yield('header')
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

          @if(session()->has('flash'))
          <div class="alert alert-success"> {{session('flash')}}</div>
          @endif

          @yield('content')
      


      </div>
    </section>
  </div>



  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Desarrollado por: <a href="https://peanut.agency">Peanut Agency</a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ now()->year }} </strong> {{config('app.name')}}
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->


<!-- jQuery -->
<script src="/adminlte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>



@include('admin.posts.create')
@include('admin.condos.create')



@stack('scripts')
<!-- AdminLTE App -->
<script src="/adminlte/js/adminlte.min.js"></script>




</body>
</html>
