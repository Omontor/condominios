<?php

use Illuminate\Database\Seeder;
use App\Incident;
use Carbon\Carbon;

class IncidentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	Incident::truncate();

         $faker = \Faker\Factory::create();


         for($i = 0; $i < 30; $i++) {

        $Incident = new Incident;
        $Incident->date  = Carbon::now()->subDays($i);
        $Incident->member_id = rand(1,125);
        $Incident->description =  $faker->paragraph;
        $Incident->image_url = $faker->url;
        $Incident->status = rand(0,1);                
        $Incident->save();
    }




    }
}
