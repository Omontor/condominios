<?php

use Illuminate\Database\Seeder;
use App\Service;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Service::truncate();

	$servicio = new Service;
	$servicio->name = 'Uber';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Didi';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Cabify';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Beat';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Radio Taxi';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Uber Eats';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Didi Food';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Rappi';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Sin Delantal';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Amazon';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Fedex';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Estafeta';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Correos de México';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Amazon';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Fumigación';
	$servicio->save();

	$servicio = new Service;
	$servicio->name = 'Tapicería';
	$servicio->save();

    }
}
