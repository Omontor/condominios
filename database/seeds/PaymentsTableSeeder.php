<?php

use Illuminate\Database\Seeder;
use App\Payment;
use App\Property;
use Carbon\Carbon;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 		Payment::truncate();

        $Pago = new Payment;
        $Pago->property_id = 1;
        $Pago->payment_date = Carbon::now();
        $Pago->payment_amount = Property::whereid($Pago->property_id)->get()->first()->monthly_maintenance;
        $Pago->save();


        $Pago = new Payment;
        $Pago->property_id = 3;
        $Pago->payment_date = Carbon::now()->subDays(4);
        $Pago->payment_amount = Property::whereid($Pago->property_id)->get()->first()->monthly_maintenance;
        $Pago->save();


        $Pago = new Payment;
        $Pago->property_id = 4;
        $Pago->payment_date = Carbon::now();
        $Pago->payment_amount = Property::whereid($Pago->property_id)->get()->first()->monthly_maintenance;
        $Pago->save();


        $Pago = new Payment;
        $Pago->property_id = 5;
        $Pago->payment_date = Carbon::now()->subDays(4);
        $Pago->payment_amount = Property::whereid($Pago->property_id)->get()->first()->monthly_maintenance;
        $Pago->save();


        $Pago = new Payment;
        $Pago->property_id = 7;
        $Pago->payment_date = Carbon::now()->subDays(5);
        $Pago->payment_amount = Property::whereid($Pago->property_id)->get()->first()->monthly_maintenance;
        $Pago->save();


        $Pago = new Payment;
        $Pago->property_id = 8;
        $Pago->payment_date = Carbon::now();
        $Pago->payment_amount = Property::whereid($Pago->property_id)->get()->first()->monthly_maintenance;
        $Pago->save();


        $Pago = new Payment;
        $Pago->property_id = 9;
        $Pago->payment_date = Carbon::now()->subDays(8);
        $Pago->payment_amount = Property::whereid($Pago->property_id)->get()->first()->monthly_maintenance;
        $Pago->save();

        $Pago = new Payment;
        $Pago->property_id = 10;
         Carbon::now()->subDays(9);
        $Pago->payment_amount = Property::whereid($Pago->property_id)->get()->first()->monthly_maintenance;
        $Pago->save();


        $Pago = new Payment;
        $Pago->property_id = 11;
        $Pago->payment_date = Carbon::now();
        $Pago->payment_amount = Property::whereid($Pago->property_id)->get()->first()->monthly_maintenance;
        $Pago->save();


        $Pago = new Payment;
        $Pago->property_id = 12;
        $Pago->payment_date = Carbon::now()->subDays(1);
        $Pago->payment_amount = Property::whereid($Pago->property_id)->get()->first()->monthly_maintenance;
        $Pago->save();
    }
}
