<?php

use Illuminate\Database\Seeder;
use App\Listing;

class ListingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Listing::truncate();
         $faker = \Faker\Factory::create();

         for($i = 0; $i < 195; $i++) {


        $Listing = new Listing;
        $Listing->title  = $faker->company;
        $Listing->image_url  ='/img/market.png';        
        $Listing->description =  $faker->paragraph;
        $Listing->address =  $faker->address;
        $Listing->lat =  $faker->numberBetween($min = 21.862202, $max = 20.9386578);  
        $Listing->lng =  $faker->numberBetween($min = -107.6359911, $max = -89.129018);
        $Listing->subdivision_id =  $faker->numberBetween($min = 1, $max = 15);
        $Listing->save();

            }


    }
}
