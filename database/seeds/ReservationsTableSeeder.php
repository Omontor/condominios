<?php

use Illuminate\Database\Seeder;
use App\Reservation;
use Carbon\Carbon;

class ReservationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Reservation::truncate();

         $faker = \Faker\Factory::create();
         for($i = 0; $i < 12; $i++) {

        $Reservation = new Reservation;
        $Reservation->start_date  = Carbon::now()->subDays($i);
        $Reservation->end_date  = $Reservation->start_date;
        $Reservation->start_time  = Carbon::now()->toDateTimeString();
        $Reservation->end_time  = Carbon::now()->toDateTimeString();
        $Reservation->member_id = rand(1,125);
        $Reservation->area_id = rand(1,5);       
        $Reservation->reservation_name =  'Cumpleaños' ." ".$faker->firstName; ;       
        $Reservation->reservation_description =  $faker->paragraph;     
        $Reservation->duration = rand(1,5);        
        $Reservation->save();
    }
}
}
