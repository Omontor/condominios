<?php

use Illuminate\Database\Seeder;
use App\CommonArea;

class CommonAreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CommonArea::truncate();

        $CommonArea = new CommonArea;
    	$CommonArea->name = "Alberca";
    	$CommonArea->save();

        $CommonArea = new CommonArea;
    	$CommonArea->name = "Roof Garden";
    	$CommonArea->save();

        $CommonArea = new CommonArea;
    	$CommonArea->name = "Cancha de Football";
    	$CommonArea->save();

        $CommonArea = new CommonArea;
    	$CommonArea->name = "Cancha de Basketball";
    	$CommonArea->save();

        $CommonArea = new CommonArea;
    	$CommonArea->name = "Salón de Usos Múltiples";
    	$CommonArea->save();

        $CommonArea = new CommonArea;
    	$CommonArea->name = "Patio";
    	$CommonArea->save();



    }
}
