<?php

use Illuminate\Database\Seeder;
use App\Debt;
use App\Property;
use Carbon\Carbon;


class DebtsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Debt::truncate();

        $Deuda = new Debt;
        $Deuda->property_id = 2;
        $Deuda->debt_date = Carbon::now()->startOfMonth();
        $Deuda->monthly_maintenance = Property::whereid($Deuda->property_id)->get()->first()->monthly_maintenance;
        $Deuda->save();


        $Deuda = new Debt;
        $Deuda->property_id = 6;
        $Deuda->debt_date = Carbon::now()->startOfMonth();
        $Deuda->monthly_maintenance = Property::whereid($Deuda->property_id)->get()->first()->monthly_maintenance;
        $Deuda->save();


    }
}
