<?php

use Illuminate\Database\Seeder;
use App\Subdivision;
use Carbon\Carbon;


class SubdivisionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subdivision::truncate();


        $Subdivision = new Subdivision;
        $Subdivision->name = 'Comida Rápida';
		$Subdivision->division_id = 1;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();

		$Subdivision = new Subdivision;
        $Subdivision->name = 'Food Trucks';
		$Subdivision->division_id = 1;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();

		$Subdivision = new Subdivision;
        $Subdivision->name = 'Pizzerías';
		$Subdivision->division_id = 1;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();

        $Subdivision = new Subdivision;
        $Subdivision->name = 'Fumigación';
		$Subdivision->division_id = 2;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();

		$Subdivision = new Subdivision;
        $Subdivision->name = 'Carpinteros';
		$Subdivision->division_id = 2;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();

		$Subdivision = new Subdivision;
        $Subdivision->name = 'Electricistas';
		$Subdivision->division_id = 2;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();

        $Subdivision = new Subdivision;
        $Subdivision->name = 'Paseadores de Perros';
		$Subdivision->division_id = 3;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();

		$Subdivision = new Subdivision;
        $Subdivision->name = 'Veterinarios';
		$Subdivision->division_id = 3;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();

		$Subdivision = new Subdivision;
        $Subdivision->name = 'Estéticas Caninas';
		$Subdivision->division_id = 3;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();

        $Subdivision = new Subdivision;
        $Subdivision->name = 'Clases de Regularización';
		$Subdivision->division_id = 4;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();

		$Subdivision = new Subdivision;
        $Subdivision->name = 'Idiomas';
		$Subdivision->division_id = 4;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();

		$Subdivision = new Subdivision;
        $Subdivision->name = 'Educación a Distancia';
		$Subdivision->division_id = 4;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();

        $Subdivision = new Subdivision;
        $Subdivision->name = 'Estéticas';
		$Subdivision->division_id = 5;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();

		$Subdivision = new Subdivision;
        $Subdivision->name = 'Uñas';
		$Subdivision->division_id = 5;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();

		$Subdivision = new Subdivision;
        $Subdivision->name = 'Maquillistas';
		$Subdivision->division_id = 5;
		$Subdivision->created_at = Carbon::now();
		$Subdivision->save();


    }
}
