<?php

use Illuminate\Database\Seeder;
use App\Division;

class DivisionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Division::truncate();

        $division = new Division;
        $division->name = 'Alimentos';
        $division->save();

        $division = new Division;
        $division->name = 'Mantenimiento del Hogar';
        $division->save();

        $division = new Division;
        $division->name = 'Mascotas';
        $division->save();

        $division = new Division;
        $division->name = 'Enseñanza';
        $division->save();

        $division = new Division;
        $division->name = 'Salud y Belleza';
        $division->save();
    }
}
