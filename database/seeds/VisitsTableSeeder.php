<?php

use Illuminate\Database\Seeder;
use App\Visit;
use App\Service;
use Carbon\Carbon;

class VisitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Visit::truncate();

        $faker = \Faker\Factory::create();


         for($i = 0; $i < 25; $i++) {

        $Visit = new Visit;
        $Visit->name  = $faker->firstName ." " .$faker->lastName;
        $Visit->property_id = rand(1,12);
        $Visit->next_visit = Carbon::now()->addDays(5);
        $Visit->last_visit = Carbon::now()->subDays(12);                
        $Visit->save();
        $random = rand(1,11);
        $Service = Service::find($random);   
        $Visit->service()->attach($Service);
    }

    }
}
