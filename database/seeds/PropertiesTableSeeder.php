<?php

use Illuminate\Database\Seeder;
use App\Property;
use App\Condo;
use Carbon\Carbon;


class PropertiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     Property::truncate();

     $Property = new Property;
     $Property->condo_id = 1;
     $Property->address = 'San Borja 633 Int 301';
     $Property->area = "B";
     $Property->members = 4;    
     $Property->has_payed = true; 
     $Property->monthly_maintenance = Condo::whereid($Property->condo_id)->get()->first()->monthly_maintenance;
     
     $Property->save();
     

     $Property = new Property;
          $Property->condo_id = 1;

     $Property->address = 'San Borja 633 Int 302';
     $Property->area = "B";
     $Property->members = 4;    
     $Property->has_payed = false;
     $Property->monthly_maintenance = Condo::whereid($Property->condo_id)->get()->first()->monthly_maintenance;
     $Property->save();
     $Property = new Property;
     $Property->condo_id = 1;

     $Property->address = 'San Borja 633 Int 303';
          $Property->area = "B";
     $Property->members = 4;    
     $Property->has_payed = true; 
          $Property->monthly_maintenance = Condo::whereid($Property->condo_id)->get()->first()->monthly_maintenance; 
     $Property->save();

     $Property = new Property;
          $Property->condo_id = 1;

     $Property->address = 'San Borja 633 Int 304';
          $Property->area = "B";
     $Property->members = 4;    
     $Property->has_payed = true;  
          $Property->monthly_maintenance = Condo::whereid($Property->condo_id)->get()->first()->monthly_maintenance;
     $Property->save();

     $Property = new Property;
          $Property->condo_id = 1;

     $Property->address = 'San Borja 633 Int 305';
          $Property->area = "B";
     $Property->members = 4;    
     $Property->has_payed = true;  
          $Property->monthly_maintenance = Condo::whereid($Property->condo_id)->get()->first()->monthly_maintenance;
     $Property->save();

     $Property = new Property;
     $Property->condo_id = 1;
     $Property->address = 'San Borja 633 Int 306';
     $Property->area = "B";
     $Property->members = 4;    
     $Property->has_payed = false;  
     $Property->monthly_maintenance = Condo::whereid($Property->condo_id)->get()->first()->monthly_maintenance;
     $Property->save();


//Propiedades de Condominio 2

     $Property = new Property;
     $Property->condo_id = 2;
     $Property->address = 'Interior 1';
     $Property->area = "Edificio 1";
     $Property->members = 5;    
     $Property->has_payed = true;  
     $Property->monthly_maintenance = Condo::whereid($Property->condo_id)->get()->first()->monthly_maintenance;
     $Property->save();


     $Property = new Property;
     $Property->condo_id = 2;
     $Property->address = 'Interior 2';
     $Property->area = "Edificio 1";
     $Property->members = 5;    
     $Property->has_payed = true;  
     $Property->monthly_maintenance = Condo::whereid($Property->condo_id)->get()->first()->monthly_maintenance;
     $Property->save();

     $Property = new Property;
     $Property->condo_id = 2;
     $Property->address = 'Interior 2';
     $Property->area = "Edificio 1";
     $Property->members = 5;    
     $Property->has_payed = true;  
     $Property->monthly_maintenance = Condo::whereid($Property->condo_id)->get()->first()->monthly_maintenance;
     $Property->save();

     $Property = new Property;
     $Property->condo_id = 2;
     $Property->address = 'Interior 2';
     $Property->area = "Edificio 1";
     $Property->members = 5;    
     $Property->has_payed = true;  
     $Property->monthly_maintenance = Condo::whereid($Property->condo_id)->get()->first()->monthly_maintenance;
     $Property->save();

     // Propiedades de Condominio 3

     $Property = new Property;
     $Property->condo_id = 3;
     $Property->address = 'Barrio del agua 32';
     $Property->area = "Barrio del agua";
     $Property->members = 3;    
     $Property->has_payed = true;  
     $Property->monthly_maintenance = Condo::whereid($Property->condo_id)->get()->first()->monthly_maintenance;
     $Property->save();



     $Property = new Property;
     $Property->condo_id = 3;
     $Property->address = 'Paseo de las cadenas 12';
     $Property->area = "Zona Esmeralda";
     $Property->members = 6;    
     $Property->has_payed = true;  
     $Property->monthly_maintenance = Condo::whereid($Property->condo_id)->get()->first()->monthly_maintenance;
     $Property->save();

    }
}
