<?php

use Illuminate\Database\Seeder;
use App\Visitor;
use Carbon\Carbon;

class VisitorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Visitor::truncate();
        $faker = \Faker\Factory::create();


         for($i = 0; $i < 88; $i++) {

        $visitor = new Visitor;
        $visitor->name  = $faker->firstName ." " .$faker->lastName;
        $visitor->property_id = rand(1,12);
        $visitor->next_visit = Carbon::now()->addDays(2);
        $visitor->last_visit = Carbon::now()->subDays(12);                
        $visitor->save();
    }


    }
}
