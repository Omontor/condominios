<?php

use Illuminate\Database\Seeder;
use App\Condo;
use App\CommonArea;
use Carbon\Carbon;

class CondosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     

    	Condo::truncate();

        $SamBorga = new Condo;
        $SamBorga->name = 'Condominio San Borja 633';
        $SamBorga->logo_url = '/img/villa.png';
        $SamBorga->address= 'San Borja 633. Colonia del Valle';
        $SamBorga->lat= "19.3848122";
        $SamBorga->lng= "-99.1666264";
        $SamBorga->primary_color= "FFDE38";
        $SamBorga->secondary_color= "FF3838";      
        $SamBorga->monthly_maintenance= 900; 
 
        $SamBorga->save();
        $commonArea = CommonArea::find(1);   
        $SamBorga->common_areas()->attach($commonArea);
        $commonArea = CommonArea::find(2);   
        $SamBorga->common_areas()->attach($commonArea);
        $commonArea = CommonArea::find(3);   
        $SamBorga->common_areas()->attach($commonArea);
        $commonArea = CommonArea::find(4);   
        $SamBorga->common_areas()->attach($commonArea);

        $guirasoles = new Condo;
        $guirasoles->name = 'Condominio "Las Margaritas"';
        $guirasoles->logo_url = '/img/villa.png';
        $guirasoles->address= 'Av. Nizuc 27. Cancún, Qroo';
        $guirasoles->lat= "21.1462597";
        $guirasoles->lng= "-86.8331803";
        $guirasoles->primary_color= "009CDF";
        $guirasoles->secondary_color= "000000";
        $guirasoles->monthly_maintenance= 1200;         
        $guirasoles->save();
        $commonArea = CommonArea::find(3);   
        $guirasoles->common_areas()->attach($commonArea);
        $commonArea = CommonArea::find(4);   
        $guirasoles->common_areas()->attach($commonArea);



        $lasfaincs = new Condo;
        $lasfaincs->name = 'Residencial "Las Fincas"';
        $lasfaincs->logo_url = '/img/villa.png';
        $lasfaincs->address= 'Las Fincas, Jiutepec Morelos';
        $lasfaincs->lat= "18.8825089";
        $lasfaincs->lng= "-99.1817213";
        $lasfaincs->primary_color= "009CDF";
        $lasfaincs->secondary_color= "000000";
        $lasfaincs->monthly_maintenance= 1000;
        $lasfaincs->save();
                $commonArea = CommonArea::find(1);   
        $lasfaincs->common_areas()->attach($commonArea);
        $commonArea = CommonArea::find(5);   
        $lasfaincs->common_areas()->attach($commonArea);


    }
}
