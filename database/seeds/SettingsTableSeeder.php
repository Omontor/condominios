<?php

use Illuminate\Database\Seeder;
use App\Setting;
class SettingsTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Setting::truncate();
        $faker = \Faker\Factory::create();

       $setting = new Setting;
       $setting->site_name = 'Mi blog';
	     $setting->site_desc = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer viverra justo nec auctor ornare. Quisque tristique et purus a commodo. Maecenas sodales risus quis mollis gravida. Praesent egestas rhoncus augue. Ut suscipit viverra velit, nec laoreet nisi euismod ac. Pellentesque maximus eros neque, at pretium eros cursus in. Donec et tortor erat. Maecenas lectus urna, vulputate in tristique a, bibendum et mi. Pellentesque elementum dolor urna. Nulla libero diam, mattis sed sapien sit amet, luctus interdum nunc.';
	   $setting->admin_email = 'admin@email.com';
	   $setting->admin_name = $faker->firstName ." " .$faker->lastName;
     $setting->email_from = $faker->email;
	   $setting->email_host = 'olivermontor.com';
	   $setting->email_port = '465';
	   $setting->email_username = 'emails@olivermontor.com';	
	   $setting->email_password = 'Monsa1980!';	   
       $setting->save();
}
}
