<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->call(PostsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(CommonAreasTableSeeder::class); 
        $this->call(CondosTableSeeder::class);
        $this->call(PropertiesTableSeeder::class);
        $this->call(DebtsTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);  
        $this->call(MembersTableSeeder::class);
        $this->call(VisitorsTableSeeder::class); 
        $this->call(IncidentsTableSeeder::class);         
        $this->call(DivisionsTableSeeder::class);        
        $this->call(SubdivisionsTableSeeder::class);                       
        $this->call(ListingsTableSeeder::class);                       
        $this->call(EventsTableSeeder::class);
        $this->call(ReservationsTableSeeder::class); 
        $this->call(ServicesTableSeeder::class); 
        $this->call(VisitsTableSeeder::class); 


        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
 