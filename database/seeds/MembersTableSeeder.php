<?php

use Illuminate\Database\Seeder;
use App\Member;
use App\User;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
$users = User::whereHas('roles', function($q){
    $q->where('name', 'Member');
})->get();
        foreach ($users as $user)
		{
			$member = new Member;
			$member->user_id = $user->id;
			$member->property_id = rand(1,12);
			$member->save();

		}

    }
}
