<?php

use Illuminate\Database\Seeder;
use App\Event;
use Carbon\Carbon;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Event::truncate();

        $faker = \Faker\Factory::create();
         for($i = 0; $i < 12; $i++) {

        $Event = new Event;
        $Event->start_date  = Carbon::now()->subDays($i);
        $Event->end_date  = $Event->start_date;
        $Event->condo_id = rand(1,3);
        $Event->event_name =  'asamblea';       
        $Event->event_description =  $faker->paragraph;     
        $Event->duration = rand(1,5);        
        $Event->save();

            }
    }

    
}
