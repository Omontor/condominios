<?php

use Illuminate\Database\Seeder;
use App\Project;
use App\ProjectCategory;
use Carbon\Carbon;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	ProjectCategory::truncate();
       Project::truncate();


        $ProjectCategory = new ProjectCategory;
    	$ProjectCategory->name = "Categoría 1";
    	$ProjectCategory->save();



    	$ProjectCategory = new ProjectCategory;
    	$ProjectCategory->name = "Categoría 2";
    	$ProjectCategory->save();


        $ProjectCategory = new ProjectCategory;
        $ProjectCategory->name = "Categoría 3";
        $ProjectCategory->save();


        $ProjectCategory = new ProjectCategory;
        $ProjectCategory->name = "Categoría 4";
        $ProjectCategory->save();


        $ProjectCategory = new ProjectCategory;
        $ProjectCategory->name = "Categoría 5";
        $ProjectCategory->save();


       $proyecto1 = new Project;
       $proyecto1->title = 'Proyecto 1';
       $proyecto1->url = str_slug($proyecto1->title);
       $proyecto1->body = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur scelerisque, nisl et fringilla fringilla, lorem risus blandit sem, vitae mollis ipsum ligula eu nunc. Pellentesque mattis neque non accumsan hendrerit. Pellentesque accumsan mauris ac purus ultrices, non porta arcu finibus. Mauris risus massa, ultricies vel iaculis at, maximus ut sem. Phasellus sed nulla sodales, scelerisque erat vel, feugiat diam. Donec luctus nec libero nec vulputate. Nam eget porta ex. Sed id ipsum vel mauris dictum mollis. Nam felis neque, auctor sed mi vel, fermentum euismod ligula. Suspendisse placerat volutpat turpis, hendrerit consequat leo sodales at. Proin nulla magna, auctor suscipit accumsan ac, cursus sit amet ipsum. Fusce imperdiet dictum fringilla.';
       $proyecto1->youtube_id = 'UTwgInKmEHI';
       $proyecto1->created_at = Carbon::now()->subDays(4);
       $proyecto1->category_id = 1;
       $proyecto1->save();

       $proyecto2 = new Project;
       $proyecto2->title = 'Proyecto 2';
       $proyecto2->url = str_slug($proyecto2->title);
       $proyecto2->body = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur scelerisque, nisl et fringilla fringilla, lorem risus blandit sem, vitae mollis ipsum ligula eu nunc. Pellentesque mattis neque non accumsan hendrerit. Pellentesque accumsan mauris ac purus ultrices, non porta arcu finibus. Mauris risus massa, ultricies vel iaculis at, maximus ut sem. Phasellus sed nulla sodales, scelerisque erat vel, feugiat diam. Donec luctus nec libero nec vulputate. Nam eget porta ex. Sed id ipsum vel mauris dictum mollis. Nam felis neque, auctor sed mi vel, fermentum euismod ligula. Suspendisse placerat volutpat turpis, hendrerit consequat leo sodales at. Proin nulla magna, auctor suscipit accumsan ac, cursus sit amet ipsum. Fusce imperdiet dictum fringilla.';
       $proyecto2->vimeo_id = '130278826';
       $proyecto2->created_at = Carbon::now()->subDays(4);
       $proyecto2->category_id = 1;
       $proyecto2->save();


       $proyecto3 = new Project;
       $proyecto3->title = 'Proyecto 3';
       $proyecto3->url = str_slug($proyecto3->title);
       $proyecto3->body = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur scelerisque, nisl et fringilla fringilla, lorem risus blandit sem, vitae mollis ipsum ligula eu nunc. Pellentesque mattis neque non accumsan hendrerit. Pellentesque accumsan mauris ac purus ultrices, non porta arcu finibus. Mauris risus massa, ultricies vel iaculis at, maximus ut sem. Phasellus sed nulla sodales, scelerisque erat vel, feugiat diam. Donec luctus nec libero nec vulputate. Nam eget porta ex. Sed id ipsum vel mauris dictum mollis. Nam felis neque, auctor sed mi vel, fermentum euismod ligula. Suspendisse placerat volutpat turpis, hendrerit consequat leo sodales at. Proin nulla magna, auctor suscipit accumsan ac, cursus sit amet ipsum. Fusce imperdiet dictum fringilla.';
       $proyecto3->vimeo_id = '50108811';
       $proyecto3->created_at = Carbon::now()->subDays(4);
       $proyecto3->category_id = 1;
       $proyecto3->save();


       $proyecto4 = new Project;
       $proyecto4->title = 'Proyecto 4';
       $proyecto4->url = str_slug($proyecto4->title);
       $proyecto4->body = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur scelerisque, nisl et fringilla fringilla, lorem risus blandit sem, vitae mollis ipsum ligula eu nunc. Pellentesque mattis neque non accumsan hendrerit. Pellentesque accumsan mauris ac purus ultrices, non porta arcu finibus. Mauris risus massa, ultricies vel iaculis at, maximus ut sem. Phasellus sed nulla sodales, scelerisque erat vel, feugiat diam. Donec luctus nec libero nec vulputate. Nam eget porta ex. Sed id ipsum vel mauris dictum mollis. Nam felis neque, auctor sed mi vel, fermentum euismod ligula. Suspendisse placerat volutpat turpis, hendrerit consequat leo sodales at. Proin nulla magna, auctor suscipit accumsan ac, cursus sit amet ipsum. Fusce imperdiet dictum fringilla.';
       $proyecto4->iframe = '<iframe src="https://alteredqualia.com/three/examples/webgl_pasta.html" width = 100%>';
       $proyecto4->created_at = Carbon::now()->subDays(4);
       $proyecto4->category_id = 1;
       $proyecto4->save();


    }
}
